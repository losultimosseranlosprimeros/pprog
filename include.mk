ODIR=$(TOP)bin/obj
FLAGS = $(CUSTOMFLAGS) -g -Wall -I $(TOP)libs/gdsl/src/ -I $(TOP)libs/jansson/src/

OBJS = $(patsubst %,$(ODIR)/%,$(_OBJ))


SUBLIB = $(patsubst %, $(ODIR)/lib%.a, $(SUBDIR))
LIB = $(ODIR)/lib$(MODULE).a

all: $(LIB) $(SUBLIB)

.PRECIOUS: $(ODIR)/%.o
.PHONY: clean

$(ODIR)/%.o:	%.c %.h
	@gcc -c $< $(FLAGS) -o $@

$(LIB):	$(OBJS) $(SUBLIB)
	@ar -r $@ $^
	@echo $(SUBLIB)
	@echo Compiled $(MODULE)

$(ODIR)/lib%.a:	 %/module force_look
	@

%/module:	force_look
	@cd $(@D); $(MAKE)

clean:
	@echo cleaning $(MODULE)
	@-rm -f $(OBJS) *~ core $(LIB)
	@-for d in $(SUBDIR); do (cd $$d; $(MAKE) clean ); done

force_look:
	@true