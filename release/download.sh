echo "Downloading game from bitbucket
"
git init
git remote add gameUpdate http://bitbucket.org/losultimosseranlosprimeros/pprog.git
git fetch --tags --depth 1
version="`git describe --tags \`git rev-list --tags --max-count=1\``"
git checkout $version
echo "

Game is now ready:
"
echo "Latest version has been downloaded: " $version
echo "
to play run make

to update run make update

for more commands: make help
"
