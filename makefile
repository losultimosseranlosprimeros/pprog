ODIR=bin/obj
FLAGS=-g -Wall -I $(TOP)libs/gdsl/src/ -I $(TOP)libs/ncurses/include/ -I $(TOP)libs/jansson/src/
LIBS= -lncurses -lm

_OBJ = main.o general.o
OBJS = $(patsubst %,$(ODIR)/%,$(_OBJ))

_DIRS = interface commandInterpreter controllers game common logic 
DIRS = $(patsubst %,src/%,$(_DIRS))
MODULES = $(patsubst %, $(ODIR)/lib%.a, $(_DIRS))
#MODULES = $(join $(DIRS), $(patsubst %, /lib%.a, $(_DIRS)))

export FLAGS

all: main

.PRECIOUS: $(ODIR)/%.o $(ODIR)/tests/%.o tests/%.exe
.PHONY: clean main force_look valgrind valgrind-mem bundle libraries gsdl resources ncurses jansson help

main: bundle
	-cd build;./main.exe
	@echo "\nTo run again enter cd build"
	@echo "Then ./main.exe\n"

bundle: build/main.exe resources tools/jsonCompressor
	@cat resources/world.json > tools/jsonCompressor > build/resources/world.json

resources:
	@rsync -rupE resources build/

$(ODIR)/main.o:	src/main.c
	@gcc -c $< $(FLAGS) -o $@

$(ODIR)/%.o:	src/%.c src/%.h
	@gcc -c $< $(FLAGS) -o $@

build/main.exe: libraries $(ODIR)/main.a
	@mkdir -p build
	@gcc $(ODIR)/*.o libs/gdsl_compiled/libgdsl.a libs/jansson_compiled/libjansson.a $(FLAGS) $(LIBS) -o $@

$(ODIR)/main.a:	$(MODULES) $(OBJS)
	@ar -r $@ $^

$(ODIR)/lib%.a:	 src/%/module force_look
	@

%/module:	force_look
	@cd $(@D); $(MAKE)



###########################
#        LIBRARIES        #
###########################

libraries:	gdsl jansson

gdsl:	libs/gdsl_compiled/libgdsl.a
	@

ncurses: libs/ncurses_compiled/libncurses.a
	@

jansson: libs/jansson_compiled/libjansson.a
	@

libs/gdsl_compiled/libgdsl.a:
	@cp -R libs/gdsl libs/gdsl_temp
	@cd libs/gdsl_temp; ./configure; make
	@mv libs/gdsl_temp/src/.libs libs/gdsl_compiled
	@-rm -r libs/gdsl_temp
	@echo "\n\nCompiled gdsl library\n\n"

libs/ncurses_compiled/libncurses.a:	
	@cp -R libs/ncurses libs/ncurses_temp
	@cd libs/ncurses_temp; ./configure; make;
	@-cd libs/ncurses_temp; make install.data
	@mkdir -p libs/ncurses_compiled
	@mv libs/ncurses_temp/lib/libncurses.a libs/ncurses_compiled/libncurses.a
	@-rm -r libs/ncurses_temp
	@echo "\n\nCompiled ncurses library\n\n"

libs/jansson_compiled/libjansson.a:	
	@cp -R libs/jansson libs/jansson_temp
	@cd libs/jansson_temp; ./configure; make;
	@mkdir -p libs/jansson_compiled
	@mv libs/jansson_temp/src/.libs/libjansson.a libs/jansson_compiled/libjansson.a
	@-rm -r libs/jansson_temp
	@echo "\n\nCompiled jansson library\n\n"


###########################
#        DEBUGGING        #
###########################

clean: clean-keep-bundle
	@-rm -rf build

clean-keep-bundle:
	@-rm -f $(OBJS) $(ODIR)/tests/*.o *~ core tests/*.exe *.log $(ODIR)/main.a
	@-for d in $(DIRS); do (cd $$d; $(MAKE) clean ); done

force_look:
	@true

valgrind: bundle
	@echo "" > valgrind.log
	@xterm -e tail -f valgrind.log &
	@-cd build; valgrind --dsymutil=yes --log-file=../valgrind.log ./main.exe

valgrind-mem: bundle
	@echo "" > valgrind-mem.log
	@xterm -e tail -f valgrind-mem.log &
	@-cd build; valgrind --leak-check=full --log-file=../valgrind-mem.log ./main.exe

valgrind-mem-show: bundle
	@echo "" > valgrind-mem.log
	@xterm -e tail -f valgrind-mem.log &
	@-cd build; valgrind --leak-check=full --show-reachable=yes --log-file=../valgrind-mem.log ./main.exe

log: bundle
	@echo "" > build/log.txt
	@xterm -e tail -f build/log.txt &
	@-cd build; ./main.exe

xterm:	bundle
	@-cd build; xterm -b 0 -e ./main.exe

xCodeBundle:	build/xcode.exe
	@

build/xcode.exe:	FLAGS += -DXCODE=42
build/xcode.exe:	force_look
	$(MAKE) clean
	$(MAKE) -e bundle
	mv build/main.exe build/xcode.exe
	$(MAKE) clean-keep-bundle

xCode:	xCodeBundle
	@echo "cd ~/pprog/proyecto/build/ \n ./xcode.exe \n exit" > build/xcode.sh
	@chmod +x build/xcode.sh
	@cd build; open -a Terminal.app xcode.sh



###########################
#          TOOLS          #
###########################

tools/jsonCompressor:	tools/jsonCompressor.c
	@gcc tools/jsonCompressor.c -o tools/jsonCompressor


$(ODIR)/tests/%.o:	tests/%.c tests/%.h tests/testgeneral.h
	@gcc -c $< $(FLAGS) -o $@

test_%:	tests/%.exe
	@-./$^

tests/%.exe:	$(ODIR)/%.o $(ODIR)/tests/%test.o
	@gcc $^ $(FLAGS) $(LIBS) -o $@

help:	force_look
	@echo "make: compiles (if necessary) and runs the game."
	@echo "make log: runs the game with an additional logging window."
	@echo "make valgrind: runs the game with an additional valgrind window."
	@echo "./ncursesFix.sh run if ncurses installation failed."

update:
	@-git remote add gameUpdate http://bitbucket.org/losultimosseranlosprimeros/pprog.git
	@git fetch --tags --depth 1
	@version="`git describe --tags \`git rev-list --tags --max-count=1\``";git checkout $$version
