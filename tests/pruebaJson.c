#include "../libs/jansson/src/jansson.h"
#include <stdio.h>

int main(){

	json_t *root;
	json_error_t error;

	root = json_load_file("../resources/world.json", 0, &error);

	if(!root)
	{
	    fprintf(stderr, "error: on line %d: %s\n", error.line, error.text);
	    return 1;
	}

	if(!json_is_array(root))
{
    fprintf(stderr, "error: root is not an array\n");
    return 1;
}
}