#include "testgeneral.h"
#include "modalwindowtest.h"
#include "../modalWindow.h"


int main(){
	
	modalWindow *mw;

	initscr();
	noecho();
	clear();

	mw = iniModalWindow(5,5,1,1,MODAL_TEXT);

	move(10,0);
	printw("Se deberia haber llamado a win_new\nPulsa enter para continuar");
	refresh();
	WAIT;

	clear();
	animateModalWindow(mw, UP);

	move(10,0);
	printw("Se deberia haber llamado a win_resize varias veces\nPulsa enter para continuar");
	refresh();
	WAIT;

	endwin();
	return 0;

}

sc_window * win_new(int a,int b,int c,int d){

	printw("Llamada a win_new\n");
	return NULL;

}

void win_delete(sc_window *sc){

	printw("Llamada a win_delete");
}


void win_refresh(sc_window *sc){

	printw("Llamada a win_refresh\n");

}

void win_resize(sc_window *sc,int r1, int c1, int nr, int nc){

	printw("Llamada a win resize con parametros: %d,%d,%d,%d",r1,c1,nr,nc);

}

void refreshMenuWindow(modalWindow * mw){

	printw("LLamada a refreshMenuWindow");

}

