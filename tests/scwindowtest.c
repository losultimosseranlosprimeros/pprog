#include "testgeneral.h"
#include "scwindowtest.h"
#include "../scwindow.h"

chtype * chstr(char *str){
	chtype *pch, *result = malloc((strlen(str)+1)*sizeof(chtype));
	for(pch=result;*str!=0;pch++,str++){
		*pch = (chtype)*str;
	}
	*pch = 0;
	return result;
}

int main(){

	sc_window * sc, *sc2;

	initscr();
	noecho();
	start_color();
	init_pair(5,COLOR_WHITE,COLOR_BLACK);
	clear();
	//signal(SIGSEGV, clean_exit_on_sig);

	sc = win_new(1,1,5,5,1);
	sc2 = win_new(1,8,5,5,2);
	win_bgcol(sc, COLOR_RED, NO);
	win_bgcol(sc2, COLOR_BLUE, NO);
	win_cls(sc);
	win_cls(sc2);

	move(10,0);
	wattron(stdscr,COLOR_PAIR(5));
	printw("Aquí encima deberían aparecer un cuadrado rojo y otro azul\nPulsa enter para continuar");
	refresh();
	WAIT;

	clear();
	win_cls(sc);
	win_write_at(sc, 2,2,chstr("hola\nprobando\n1\n2\n33333"));

	move(10,0);
	wattron(stdscr,COLOR_PAIR(5));
	printw("Ahora el rojo debería tener texto y el otro debería haber desaparecido\nPulsa enter para continuar");
	refresh();
	WAIT;

	clear();
	win_refresh(sc);

	move(10,0);
	wattron(stdscr,COLOR_PAIR(5));
	printw("Y ahora no debería haber cambiado\nPulsa enter para continuar");
	refresh();
	WAIT;

	clear();
	win_refresh(sc);
	win_bgcol(sc, COLOR_CYAN, YES);
	win_fgcol(sc, COLOR_MAGENTA, YES);
	win_write_at(sc,1,1, chstr("aaaaa"));

	move(10,0);
	wattron(stdscr,COLOR_PAIR(5));
	printw("Ahora se añade texto de otro color\nPulsa enter para continuar");
	refresh();
	WAIT;

	clear();
	win_refresh(sc);

	move(10,0);
	wattron(stdscr,COLOR_PAIR(5));
	printw("Ahora debería cambiar entera de color\nPulsa enter para continuar");
	refresh();
	WAIT;

	clear();
	win_bgcol(sc, -1, YES);
	win_fgcol(sc, -1, YES);
	win_refresh(sc);

	move(10,0);
	wattron(stdscr,COLOR_PAIR(5));
	printw("Ahora debería volver al color original entera\nPulsa enter para continuar");
	refresh();
	WAIT;



	win_delete(sc);

	endwin();

	return 0;

}