#ifndef _movementlogicheader
#define _movementlogicheader

#include "../general.h"
#include "../game/game.h"

int mlogic_movePlayer(Game *game, int dx, int dy);

int mlogic_moveEnemie (Game *game, Enemie *enemie, int dx, int dy);

Coord mlogic_terminal_origin (Game *game, int width, int height);

int doesCoordColide (Game *game, Coord curr, Coord pos);


#endif
