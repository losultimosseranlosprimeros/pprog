
#ifndef _flogicmainheader
#define _flogicmainheader

#include "../../general.h"
#include "../../game/game.h"

/**
 * To be called before the first cycle or after returning from pause
 */
void resetTimer();

bool flogic_run_cycle (Game *game,World *world);

#endif