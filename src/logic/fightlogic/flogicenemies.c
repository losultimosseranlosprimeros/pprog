#include "flogicenemies.h"
#include "../../controllers/generalcontroller.h"
#include "../../controllers/controllerinclude.h"
#include <math.h>

typedef struct{
	IA *ia;
	Game *game;
}_moveHelper;

#define max_distance(total) ((4 *total)  / 3)
#define MAX_DISTANCE_ATTEMPTS 3


int _moveEnemie(const gdsl_element_t E, gdsl_location_t LOCATION, void *USER_DATA);
int _applyDamage(const gdsl_element_t E, gdsl_location_t LOCATION, void *USER_DATA);

void flogice_move_all(Game *game, IA *ia){
	gdsl_list_t enemies=getGameEnemies(game);
	_moveHelper helper;
	helper.ia=ia;
	helper.game=game;
	gdsl_list_map_forward(enemies,_moveEnemie,&helper);
}

int _moveEnemie(const gdsl_element_t E, gdsl_location_t LOCATION, void *USER_DATA){
	_moveHelper *helper=(_moveHelper *)USER_DATA;
	Enemie *enemie=(Enemie *)E;
	Coord pos,next,diff;
	
	pos=getEnemiePos(enemie);
	next=flogicia_next_coord(helper->ia, helper->game, pos, getEnemieOri(enemie));
	diff=coordDiff(next,pos);
	mlogic_moveEnemie(helper->game, enemie, diff.x, diff.y);
	return GDSL_MAP_CONT;
}

static int enemiesToCreate = 0;

void flogice_create_enemies(Game *game, Square visible){

	int nEnemies;

	if (gdsl_list_get_size(getGameEnemies(game)) == 0 && !enemiesToCreate){
		/* Starting new Wave !!! */
		int waveNumber = incrementWaveNumber(game);
		enemiesToCreate = (int) pow(2, waveNumber+5) + rand()%(ZOMBIE_WAVE_UNIT*waveNumber);
		statusControllerDisplayMessage("Comenzada la ola %d: %d zombies",waveNumber,enemiesToCreate);
	}

	if (!enemiesToCreate) return;

	/* This formula makes zombies appear faster in higher waves */
	nEnemies = (pow(2, getGameWaveNumber(game)+5) / 10);
	nEnemies += rand() % (nEnemies/2);

	/* Search for non visible rooms */
	gdsl_list_t roomList = gdsl_list_inverse_filtered_copy(getMapRooms(getStageMap(getGameCurrentStage(game))), visibleRoomFilter, &visible);

	int actuallyGenerated = 0;
	int maxRealDistance = max_distance( coordAproxModulus(createCoord(COLS, LINES)));
	/* For every enemyToCreate */
	int i,j;
	for (i = 0; i < nEnemies; ++i){

		/* Choose a random room */
		Room *room = gdsl_list_get_random(roomList);
		if (!room){
			break;
		}

		j=0;
		while((room->zombiable == NO) || (j < MAX_DISTANCE_ATTEMPTS && coordAproxModulus(squareCenter(getRoomCoords(room))) <= maxRealDistance) ){
		//while(room->zombiable == NO){
			room = gdsl_list_get_random(roomList);
			j++;
		}

		/* Initialize counter to avoid halting in this loop if no space available */
		j = 0;
		Coord size = squareSize(createSquare(room->ulc, room->lrc));
		int maxAtempts = size.x * size.y / 2;

		/* Choose a random empty position in the room */
		Coord newPos;
		do{
			newPos = randomCoordInsideSquare(createSquare(room->ulc, room->lrc), NO);
		}while(doesCoordColide(game, newPos, newPos) && j++ < maxAtempts );

		if (--j != maxAtempts){
			/* Add enemy to list */
			actuallyGenerated++;
			Enemie * enemy = iniEnemie(newPos, rand()%4, 5, 1);
			gdsl_list_insert_head(getGameEnemies(game), enemy);
		}

	}

	enemiesToCreate -= actuallyGenerated;
	/* Due to randomness more enemies than necessarie might be created.
	 * That behaviour is intented */
	if (enemiesToCreate < 0) enemiesToCreate = 0;

}

void flogice_attack_all(Game *game){
	gdsl_list_map_forward(getGameEnemies(game),_applyDamage,game);	
}

int _applyDamage(const gdsl_element_t E, gdsl_location_t LOCATION, void *USER_DATA){
	Game *game=(Game *)USER_DATA;
	Enemie *enemie=(Enemie *)E;
	Player *player=getGamePlayer(game);
	Orientation enemieOri=getEnemieOri(enemie);
	Coord playerPos=getPlayerPos(player), enemiePos=getEnemiePos(enemie);
	if(areCoordsEqual(coordPlusOrientation(enemiePos,enemieOri),playerPos)){
		if(plogic_damage(game,-2)==DEAD)
			return GDSL_MAP_STOP;
	}
	return GDSL_MAP_CONT;
}

LIFE_STATUS flogice_damage(Enemie *enemie, int damage){
	updateEnemieLife(enemie,damage);
	Life life=getEnemieLife(enemie);
	if(life<=0)
		return DEAD;
	return ALIVE;
}