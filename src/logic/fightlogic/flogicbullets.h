
#ifndef _flogicbulletsheader
#define _flogicbulletsheader

#include "../../general.h"
#include "../../game/game.h"
#include "../../game/world.h"
#include "../movementlogic.h"
#include "../objectslogic.h"
#include "flogicenemies.h"


void flogicb_move_all(Game *game, World *world);
#endif