
#include "flogicmain.h"
#include "flogicia.h"
#include "flogicenemies.h"
#include "flogicbullets.h"

#include <sys/time.h>
#include <math.h>

/*#define flogice_move_all_period 500000
#define flogice_create_enemies_period 2000000
#define flogice_attack_all_period 250000
#define flogicb_move_all_period 10000*/

#define flogice_move_all_increment_factor 0.8
#define flogice_create_enemies_increment_factor 1
#define flogice_attack_all_increment_factor 0.8 
#define flogicb_move_all_increment_factor 1

#define executeIfTime(func, args) func##_timer += offset; if( func##_timer >= func##_period ){ func##_timer %= func##_period; func args ; hasToUpdate = YES; }

static struct timeval prevTime;

void resetTimer() {
    gettimeofday (&prevTime, NULL);
}


bool flogic_run_cycle (Game *game,World *world) {

    static int flogice_move_all_timer = 0;
    static int flogice_create_enemies_timer = 0;
    static int flogice_attack_all_timer = 0;
    static int flogicb_move_all_timer = 0;

    static int flogice_move_all_period = 500000;
    static int flogice_create_enemies_period = 2000000;
    static int flogice_attack_all_period = 250000;
    static int flogicb_move_all_period = 10000;

    static int actualWave = 0;

    while( getGameWaveNumber(game) != actualWave){
        actualWave++;
        flogice_move_all_period *= flogice_move_all_increment_factor; 
        flogice_create_enemies_period *= flogice_create_enemies_increment_factor; 
        flogice_attack_all_period *= flogice_attack_all_increment_factor; 
        flogicb_move_all_period *= flogicb_move_all_increment_factor; 
    }

    struct timeval nowTime;
    gettimeofday (&nowTime, NULL);

    int offset = timeValOffset(prevTime,nowTime);
    prevTime = nowTime;

    bool hasToUpdate = NO;

    executeIfTime (flogicb_move_all, (game, world) )
    executeIfTime (flogice_move_all, (game, getGameIA (game) ) )
    executeIfTime (flogice_create_enemies, (game, getGameVisibleSquare (game) ) )
    executeIfTime (flogice_attack_all, (game) )

    return hasToUpdate;
}