#include "flogicbullets.h"

typedef struct{
	gdsl_list_t objectsList;
	gdsl_list_t bulletColideList;
	Game *game;
}_bulletHelper;

int _moveBullet(const gdsl_element_t E, gdsl_location_t LOCATION, void *USER_DATA);

void flogicb_move_all(Game *game, World *world){
	int tamanio,i;
	Bullet *bulletAux;
	
	_bulletHelper helper;
	helper.game=game;
	helper.bulletColideList=gdsl_list_alloc("aux",NULL,free);
	helper.objectsList=getWorldObjects(world);

	gdsl_list_t bulletList=getGameBullets(game);
	if(gdsl_list_get_size(bulletList))
		gdsl_list_map_forward(bulletList,_moveBullet,&helper);
	tamanio=gdsl_list_get_size(helper.bulletColideList);
	if(tamanio){
		for(i=1;i<=tamanio;i++){
			bulletAux = gdsl_list_search_by_position(helper.bulletColideList,i);
			gdsl_list_delete(bulletList,pointerCmp,bulletAux);
		}
	}
}

int _moveBullet(const gdsl_element_t E, gdsl_location_t LOCATION, void *USER_DATA){
	_bulletHelper *helper=USER_DATA;
	gdsl_list_t objects=helper->objectsList;
	Enemie *enemieAux;
	int weaponDamage, num;
	Bullet *bullet=(Bullet *)E;
	DIRECTION dir=getBulletDir(bullet);
	Coord pos=getBulletPos(bullet);
	Coord final=coordPlusOrientation(pos,dir);	

	if(doesCoordColide(helper->game,pos,final)){
		enemieAux=gdsl_list_search(getGameEnemies(helper->game), isEnemyInCoord, &final);
		if (enemieAux){
			weaponDamage=getWeaponDamage(getObjectWeapon(getPlayerWeapon(getGamePlayer(helper->game))));
			if(flogice_damage(enemieAux,weaponDamage)==DEAD){
				num=rand()%5;
				if(num==2)
					ologic_createRandomObjectInPosition(helper->game, objects,enemieAux->pos);
				gdsl_list_delete(getGameEnemies(helper->game),pointerCmp,enemieAux);
			}
		}
		gdsl_list_insert_head(helper->bulletColideList,bullet);
		return GDSL_MAP_CONT;
	}		
	setBulletPos(bullet,final);
	return GDSL_MAP_CONT;
}