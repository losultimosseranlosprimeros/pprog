
#ifndef _flogiciaheader
#define _flogiciaheader

#include "../../general.h"
#include "../../game/game.h"

typedef struct IARoom_s{
	unsigned short **weights;
	char **directions;
	Room *room;
	struct IARoom_s ***adyacents;
}IARoom;

IA *flogicia_init(Game *game);

void flogicia_update_weights(IA *ia, Game *game);

Coord flogicia_next_coord (IA *ia, Game *game, Coord pos, Orientation ori);

void free_iaroom(void *iaroom);

#endif
