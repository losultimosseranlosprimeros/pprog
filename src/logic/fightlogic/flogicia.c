#include "flogicia.h"
#include <limits.h>

struct IARoomInitVoidHelper {
    IARoom *r;
    gdsl_list_t iaRooms;
};

typedef struct {
    IARoom *container;
    Coord pos;
    bool voidCreated;
} IAOpenNode;

#define STEP_COST 1

#define weight_for_coord(weights,coord) (weights[coord.y][coord.x])
#define direction_for_coord(directions,coord) (directions[coord.y][coord.x])

static int directions[4] = {0, 1, 2, 3}, oposites[4] = {2, 3, 0, 1};


/** PRIVATE FUNCTIONS **/

static IARoom *_adyacent_IARoom_withCoord (IARoom *iar, Coord relativePos);
static void _reset_weights_before_update (IA *ia, Game *game);
int _reset_weights_for_room (gdsl_element_t element, gdsl_location_t location, void *data);
int _flogicia_reset_void (gdsl_element_t element, gdsl_location_t location, void *data);
int _create_iaroom (gdsl_element_t element, gdsl_location_t location, void *data);
int _initialize_iaroom_adyacents (gdsl_element_t element, gdsl_location_t location, void *data);
int _flogicia_init_process_void (gdsl_element_t element, gdsl_location_t location, void *data);
long _flogicia_search_iaroom_by_room (gdsl_element_t element, void *data);

/************************/



/**********************************
 *            GENERAL             *
 *********************************/

/**
 * Basically create an IARoom for every Room in the game.
 *
 * @param  game Game in wich the IA is being created
 *
 * @return      The IA container.
 */
IA *flogicia_init (Game *game) {

    IA *ia = malloc (sizeof (IA) );
    ia->iaRooms = gdsl_list_alloc ("iaRooms", NULL, free_iaroom);
    gdsl_list_map_forward (getMapRooms (getStageMap (getGameCurrentStage (game) ) ), _create_iaroom, ia->iaRooms);
    gdsl_list_map_forward (ia->iaRooms, _initialize_iaroom_adyacents, ia->iaRooms);
    logToFile ("initialized IA\n");
    return ia;
}

void free_iaroom (void *iaroom) {
    IARoom *r=iaroom;
    free (r->weights);
    free (r->directions);
    free (r->adyacents);
    free (r);
}





/**********************************
 *        INITIALIZATION          *
 *********************************/

/**
 * The purpose of this function is to initialize and precalculate as much as possible
 * to avoid halting the main thread when updating the structures.
 *
 * @param  element  Room for which the IARoom is being created
 * @param  location Is here just to comply with the map function prototype
 * @param  data     List of IARooms in which to insert the new one.
 *
 * @return          macro to continue the mapping.
 */
int _create_iaroom (gdsl_element_t element, gdsl_location_t location, void *data) {

    int i;
    IARoom *r = malloc (sizeof (IARoom) );
    r->room = element;
    Coord size = realSquareSize (getRoomCoords (r->room) );

    /* malloc room weights */
    r->weights = malloc ( (size.y * sizeof (r->weights[0]) ) + (size.x * size.y) * sizeof (r->weights[0][0]) );
    r->weights[0] = (unsigned short *) (r->weights + size.y);
    for (i = 1; i < size.y; i++) r->weights[i] = r->weights[0] + (i * size.x);

    /* malloc room directions */
    r->directions = malloc ( (size.y * sizeof (r->directions[0]) ) + (size.x * size.y) * sizeof (r->directions[0][0]) );
    r->directions[0] = (char *) (r->directions + size.y);
    for (i = 1; i < size.y; i++) r->directions[i] = r->directions[0] + (i * size.x);

    /* malloc room adyacent pointers */
    r->adyacents = calloc (1, 4 * sizeof (IARoom **) + (2 * size.x + 2 * size.y) * sizeof (IARoom *) );
    r->adyacents[0] = (IARoom **) (r->adyacents + 4);
    r->adyacents[1] = r->adyacents[0] + size.x;
    r->adyacents[2] = r->adyacents[1] + size.x;
    r->adyacents[3] = r->adyacents[2] + size.y;

    /* Set room initial weights to maximum value (except walls) */
    memset (r->weights[0], USHRT_MAX, size.x * size.y * sizeof (r->weights[0][0]) );
    memset (r->weights[0], 0, size.x * sizeof (r->weights[0][0]) );
    memset (r->weights[size.y - 1], 0, size.x * sizeof (r->weights[0][0]) );
    for (i = 1; i < size.y; i++) r->weights[i][0] = r->weights[i][size.x - 1] = 0;

    /* Finally insert the new IARoom into the list of all IARooms */
    gdsl_list_insert_head ( (gdsl_list_t) data, r);

    return GDSL_MAP_CONT;
}

int _initialize_iaroom_adyacents (gdsl_element_t element, gdsl_location_t location, void *data) {
    /* Process voids (reset their weight to zero and set the adyacent room pointer) */
    IARoom *r = element;
    struct IARoomInitVoidHelper helper;
    helper.iaRooms = data;
    helper.r = r;
    gdsl_list_map_forward (getRoomVoids (r->room), _flogicia_init_process_void, &helper);
    return GDSL_MAP_CONT;
}

int _flogicia_init_process_void (gdsl_element_t element, gdsl_location_t location, void *data) {

    RoomVoid *rv = element;
    struct IARoomInitVoidHelper *helper = data;

    /* Search for the IARoom with the room pointed by the void */
    IARoom *adyacent = gdsl_list_search (helper->iaRooms, _flogicia_search_iaroom_by_room, rv->adyacent);
    if (adyacent) {
        int x, y;
        if (rv->pos.y == 0) {
            x = rv->pos.x;
            y = 0;
        } else if (rv->pos.y == squareSize (getRoomCoords (helper->r->room) ).y) {
            x = rv->pos.x;
            y = 1;
        } else if (rv->pos.x == 0) {
            x = rv->pos.y;
            y = 2;
        } else if (rv->pos.x == squareSize (getRoomCoords (helper->r->room) ).x) {
            x = rv->pos.y;
            y = 3;
        }
        helper->r->adyacents[y][x] = adyacent;
    }

    return GDSL_MAP_CONT;

}

long _flogicia_search_iaroom_by_room (gdsl_element_t element, void *data) {
    IARoom *iar = element;
    Room *r = data;
    if (iar->room == r) {
        return FOUND;
    }
    return NOT_FOUND;
}




/**********************************
 *            UPDATING            *
 *********************************/

void flogicia_update_weights (IA *ia, Game *game) {

    _reset_weights_before_update (ia, game);

    int i, newWeight, nodeCount = 0;
    Coord actualAbsolute, actualRelative, newAbsolute, newRelative;
    bool actualIsVoid;

    gdsl_list_t openNodes = gdsl_list_alloc ("Open Nodes", NULL, NULL);

    /* Insert the root node */
    IAOpenNode *actualNode, *newNode = malloc (sizeof (IAOpenNode) );
    newNode->pos = getPlayerPos (getGamePlayer (game) );
    newNode->container = gdsl_list_search (ia->iaRooms, _flogicia_search_iaroom_by_room, getGameCurrentRoom (game) );
    newNode->voidCreated = NO;

    if (!newNode->container || !isCoordBetweenCords (newNode->pos, newNode->container->room->ulc, newNode->container->room->lrc) ) {
        return;
    }

    gdsl_list_insert_tail (openNodes, newNode);

    /* While there are open nodes */
    while (gdsl_list_get_size (openNodes) ) {

        /* Get the first node */
        actualNode = gdsl_list_remove_head (openNodes);
        actualAbsolute = actualNode->pos;
        actualRelative = coordDiff (actualAbsolute, actualNode->container->room->ulc);
        newWeight = weight_for_coord (actualNode->container->weights, actualRelative) + STEP_COST;
        actualIsVoid = isCoordInPerimeter (actualAbsolute, getRoomCoords (actualNode->container->room) );

        /* If its a void, create its correspondant node in the adyacent room */
        /* voidCreated avoids infinite node creation */
        if (actualIsVoid == YES && actualNode->voidCreated == NO) {

            /* Create new node */
            newNode = malloc (sizeof (newNode[0]) );
            newNode->pos = actualAbsolute;
            newNode->voidCreated = YES;
            newNode->container = _adyacent_IARoom_withCoord (actualNode->container, actualRelative);
            gdsl_list_insert_tail (openNodes, newNode);

            /* Copy current nodes info */
            newRelative = coordDiff (actualAbsolute, newNode->container->room->ulc);
            weight_for_coord (newNode->container->weights, newRelative) = newWeight - STEP_COST;
            direction_for_coord (newNode->container->directions, newRelative) = direction_for_coord (actualNode->container->directions, actualRelative);

        }

        /* try to update on each direction */
        for (i = 0; i < 4; i++) {

            /* If the node being looked is a void we have to be careful and look only in valid directions */
            if (actualIsVoid == YES) {
                switch (directions[i]) {
                    case UP:
                        if (actualRelative.y == 0) continue;
                        break;
                    case DOWN:
                        if (actualRelative.y == squareSize (getRoomCoords (actualNode->container->room) ).y) continue;
                        break;
                    case LEFT:
                        if (actualRelative.x == 0) continue;
                        break;
                    case RIGHT:
                        if (actualRelative.x == squareSize (getRoomCoords (actualNode->container->room) ).x) continue;
                        break;
                }
            }

            newRelative = coordPlusOrientation (actualRelative, directions[i]);

            /* if one direction is successfull, */
            int actualWeight = weight_for_coord (actualNode->container->weights, newRelative); 
            if ( actualWeight > newWeight || ( actualWeight == newWeight && rand()%3 == 0) ) {

                /* insert a node for it */
                newNode = malloc (sizeof (newNode[0]) );
                newAbsolute = coordPlusOrientation (actualAbsolute, directions[i]);
                newNode->pos = newAbsolute;
                newNode->container = actualNode->container;
                newNode->voidCreated = NO;

                gdsl_list_insert_tail(openNodes, newNode);

                /* update its weight and direction */

                weight_for_coord (newNode->container->weights, newRelative) = newWeight;
                direction_for_coord (newNode->container->directions, newRelative) = oposites[i];


            }

        }

        /* free current node */
        free (actualNode);

        nodeCount++;
    } /** End of while **/

}

static IARoom *_adyacent_IARoom_withCoord (IARoom *iar, Coord relativePos) {
    int x, y;
    if (relativePos.y == 0) {
        x = relativePos.x;
        y = 0;
    } else if (relativePos.y == squareSize (getRoomCoords (iar->room) ).y) {
        x = relativePos.x;
        y = 1;
    } else if (relativePos.x == 0) {
        x = relativePos.y;
        y = 2;
    } else if (relativePos.x == squareSize (getRoomCoords (iar->room) ).x) {
        x = relativePos.y;
        y = 3;
    }
    return iar->adyacents[y][x];
}

/** RESET BEFORE UPDATING **/

static void _reset_weights_before_update (IA *ia, Game *game) {

    gdsl_list_map_forward (ia->iaRooms, _reset_weights_for_room, game);
}

int _reset_weights_for_room (gdsl_element_t element, gdsl_location_t location, void *data) {

    IARoom *iar = element;
    Game *game = data;
    Coord size = realSquareSize (getRoomCoords (iar->room) );
    int i;

    /* Set to maximum all non-border cells */
    memset (iar->weights[0], USHRT_MAX, size.x * size.y * sizeof (iar->weights[0][0]) );
    memset (iar->weights[0], 0, size.x * sizeof (iar->weights[0][0]) );
    memset (iar->weights[size.y - 1], 0, size.x * sizeof (iar->weights[0][0]) );
    for (i = 1; i < size.y; i++) iar->weights[i][0] = iar->weights[i][size.x - 1] = 0;

    /* Set to maximum all voids */
    struct IARoomInitVoidHelper helper;
    helper.r = iar;
    gdsl_list_map_forward (iar->room->roomVoids, _flogicia_reset_void, &helper);

    /* Set to 0 player position if in this room */
    if (iar->room == getGameCurrentRoom (game) ) {
        Coord playerRelativePos = coordDiff (getPlayerPos (getGamePlayer (game) ), iar->room->ulc);
        weight_for_coord (iar->weights, playerRelativePos) = 0;
    }

    return GDSL_MAP_CONT;
}

int _flogicia_reset_void (gdsl_element_t element, gdsl_location_t location, void *data) {
    RoomVoid *rv = element;
    struct IARoomInitVoidHelper *helper = data;
    /* Place maxValue in the weighs */
    helper->r->weights[rv->pos.y][rv->pos.x] = USHRT_MAX;

    return GDSL_MAP_CONT;
}




/**********************************
 *            RESULTS             *
 *********************************/
/* The behaviour of the zombie is defined by the following rules:
 *  - If the player is nearby he goes straight for him
 *  - If he is not that close, he might steer in the wrong direction.
 *  - Zombies might not turn around (or with little probability) 
 */

#define MAX_DISTANCE 30 
#define MIN_DISTANCE 5
#define MAX_DISPERSION ((double)4)
#define BACK_FACTOR 10

Coord flogicia_next_coord (IA *ia, Game *game, Coord pos, Orientation ori) {

	Room *room = getRoomFromCoord(pos, getStageMap(getGameCurrentStage(game)));	
	IARoom *iar = gdsl_list_search(ia->iaRooms, _flogicia_search_iaroom_by_room, room);
	int distance = weight_for_coord(iar->weights, coordDiff(pos, room->ulc));
    Orientation result = direction_for_coord(iar->directions, coordDiff(pos, room->ulc));

    if (distance < MIN_DISTANCE){
        return coordPlusOrientation(pos, result);
    }

    double probability;

    if (distance > MAX_DISTANCE){
        probability = 1 / MAX_DISPERSION;
    }else{
        probability = ((double)distance)/MAX_DISTANCE;
        probability /= MAX_DISPERSION;
    }

	if ((rand()/((double)RAND_MAX)) <= probability){
        /** This one isnt quite right, but it will do fine. Maybe zombies have a tendency to steer clockwise */
        probability = ((result + 1)%4) == oposites[ori] ? 1 : BACK_FACTOR - 1;
        probability /= BACK_FACTOR;
        if ((rand()/((double)RAND_MAX)) <= probability){
            return coordPlusOrientation(pos, (result +1)%4);
        }
        return coordPlusOrientation(pos, (result+3)%4);
	}

	return coordPlusOrientation(pos, result);
}


