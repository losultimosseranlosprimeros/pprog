
#ifndef _flogicenemiesheader
#define _flogicenemiesheader

#include "../../general.h"
#include "../../game/game.h"
#include "../movementlogic.h"
#include "../playerlogic.h"
#include "flogicia.h"


void flogice_move_all(Game *game, IA *ia);

void flogice_create_enemies(Game *game, Square visible);

void flogice_attack_all(Game *game);

LIFE_STATUS flogice_damage(Enemie *enemie, int damage);

#endif