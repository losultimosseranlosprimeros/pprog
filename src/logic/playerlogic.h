#ifndef _playerlogicheader
#define _playerlogicheader

#include "../general.h"
#include "../game/game.h"
#include "../controllers/statuscontroller.h"

STATUS plogic_shoot(Game *game);

LIFE_STATUS plogic_damage(Game *game, int damage);

bool plogic_reload(Game *game,Object *object);
void plogic_changeWeapon(Game *game, Object *obj);

#endif