
#include "../general.h"
#include "../game/game.h"

/**
 * Intenta leer que objeto hay delante del jugador, 
 * y si existe y se puede coger lo quita de la habitacion y lo mete en el inventario.
 *
 * @param  game   [description]
 * @param  player [description]
 *
 * @return        [description]
 */
STATUS ologic_pickObj(Game *game);

STATUS ologic_useObj(Game *game, int index);

STATUS ologic_leaveObj(Game *game, int index);

STATUS ologic_dropObj(Game *game);

STATUS ologic_keepObj(Game *game, bool front);

void ologic_createRandomObjectInPosition(Game *game, gdsl_list_t objects, Coord pos);
