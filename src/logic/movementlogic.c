#include "movementlogic.h"
#include "objectslogic.h"
#include "fightlogic/flogicia.h"

int _doesCoordColideInRoom (Game *game, Room *room, Coord curr, Coord pos );


int mlogic_movePlayer (Game *game, int dx, int dy) {

	Coord curr,next;
	curr = game->player->pos;
	Orientation currO = game->player->ori, nextO=currO;
	if (dx > 0){
		nextO = RIGHT;
	}else if(dx < 0){
		nextO = LEFT;
	}else if(dy < 0){
		nextO = UP;
	}else if(dy > 0){
		nextO = DOWN;
	}
	if (nextO != currO){
		game->player->ori = nextO;
        next = curr;
        return OK;
	}else{
    	next = coordSum(curr, createCoord(dx, dy));
    	if (!doesCoordColide(game, curr, next)){
    		game->player->pos = next;
    	}
    }

    Room *new= otherRoom(game, curr, next);
    if (new && isCoordBetweenCords(next, new->ulc, new->lrc)){
        setGameCurrentRoom(new, game);
    }

    ologic_keepObj(game,NO);
    flogicia_update_weights(getGameIA(game),game);

	return OK;
}

int mlogic_moveEnemie (Game *game, Enemie *enemie, int dx, int dy){

    Coord curr,next;
    curr = getEnemiePos(enemie);
    Orientation currO=getEnemieOri(enemie), nextO=currO;
    if (dx > 0){
        nextO = RIGHT;
    }else if(dx < 0){
        nextO = LEFT;
    }else if(dy < 0){
        nextO = UP;
    }else if(dy > 0){
        nextO = DOWN;
    }
    setEnemieOri(enemie,nextO);
    next=coordSum(curr, createCoord(dx, dy));
    if (!doesCoordColide(game, curr, next)){
        setEnemiePos(enemie,next);
    }

    return OK;
}

Coord mlogic_terminal_origin (Game *game, int width, int height) {
    return coordDiff (game->player->pos, createCoord (width / 2, height / 2) );

}

int doesCoordColide (Game *game, Coord curr, Coord pos){
    if(areCoordsEqual(getPlayerPos(getGamePlayer(game)),curr))
        return _doesCoordColideInRoom(game,getGameCurrentRoom(game),curr,pos);
    return _doesCoordColideInRoom(game,NULL,curr,pos);
}

/*int doesCoordColideWithEnemie(Game *game, Coord curr, Coord pos){
    if(areCoordsEqual(getPlayerPos(getGamePlayer(game)),curr))
        return _doesCoordColideInRoom(game,getGameCurrentRoom(game),curr,pos);
    return _doesCoordColideInRoom(game,NULL,curr,pos);
}*/

int _doesCoordColideInRoom (Game *game, Room *currentRoom, Coord curr, Coord pos ){

    int i;
    Room *searchedRooms[2];
    if(!currentRoom){
        Room **aux = getRoomsFromVoidCoord(pos, getStageMap(getGameCurrentStage(game)));
        searchedRooms[0] = aux[0];
        searchedRooms[1] = aux[1];
        free(aux);
    }else{
        searchedRooms[0] = currentRoom;
        searchedRooms[1] = otherRoom(game, curr, pos);
    }
    
    for (i = 0; i < 2; ++i){
        if (!searchedRooms[i]){
            continue;
        }

        Room *room = searchedRooms[i];
        RoomObject *object;
        Coord relative = coordDiff (pos, room->ulc);

        /* Checking walls first */
        if ( isCoordInPerimeter (pos, getRoomCoords (room) ) && !gdsl_list_search (getRoomVoids (room), isVoidInCoord, &relative) ) {
            return YES;
        }

        /* Now for objects */
        if ((object = gdsl_list_search (getRoomObjects (room), isObjectInCoord, &relative) )) {
            if (object->object->blocks == YES){
                return YES;
            }
        }

        /* Now for player */
        if ( areCoordsEqual(pos, getPlayerPos(getGamePlayer(game))) ) {
            return YES;
        }

    }

    /* And for enemies */
    if (gdsl_list_search (getGameEnemies(game), isEnemyInCoord, &pos)){
        return YES;
    }

    /* And for bullets */
    if (gdsl_list_search (getGameBullets(game), isBulletInCoord, &pos)){
        return YES;
    }

    return NO;

}

