#include "objectslogic.h"
#include "../general.h"
#include "../game/game.h"
#include "movementlogic.h"
#include "../controllers/controllerinclude.h"

STATUS ologic_keepObj(Game *game, bool inFront){
	Player *player=getGamePlayer(game);
	Room *currentRoom=getGameCurrentRoom(game);
	gdsl_list_t backpack=getPlayerBackpack(player);
	char *message=(char *)malloc(sizeof(char)*100);

	logToFile("Intentando guardar objeto\n");
	
	Coord nextPos;
	if(inFront == YES){
		nextPos = coordDiff(coordPlusOrientation(getPlayerPos(player), getPlayerOri(player)),currentRoom->ulc);
	}else{
		nextPos = coordDiff(getPlayerPos(player),currentRoom->ulc);
	}
	RoomObject *object=gdsl_list_search(getRoomObjects(currentRoom), isObjectInCoord, &nextPos);
	
	if(!object){
		logToFile("no hay objeto\n");
		currentRoom=otherRoom(game, getPlayerPos(player), coordPlusOrientation(getPlayerPos(player), getPlayerOri(player)));
		if(!currentRoom)
			return ERR;
		else{
			if(inFront == YES){
				nextPos=coordDiff(coordPlusOrientation(getPlayerPos(player), getPlayerOri(player)),currentRoom->ulc);
			}else{
				nextPos=coordDiff(getPlayerPos(player),currentRoom->ulc);
			}
			object=gdsl_list_search(getRoomObjects(currentRoom),isObjectInCoord,&nextPos);
			if(!object)
				return ERR;
		}
	}
	if(!isObjectEquipable(object->object))
		return ERR;
	
	sprintf(message,"Has guardado %s",getObjectDescription(object->object));
	gdsl_list_insert_head(backpack,object->object);
	statusControllerDisplayMessage(message);
	free(message);
	gdsl_list_remove(getRoomObjects(currentRoom), pointerCmp, object);

	return OK;
}



STATUS ologic_pickObj(Game *game){
	
	Player *player = getGamePlayer(game);
	if (getPlayerHand(player)){
		return ERR;
	}
	Room *currentRoom = getGameCurrentRoom(game);
	Coord looked = coordDiff(coordPlusOrientation(getPlayerPos(player), getPlayerOri(player)),currentRoom->ulc);
	RoomObject *object = gdsl_list_search(getRoomObjects(currentRoom), isObjectInCoord, &looked);
	if (!object){
		currentRoom = otherRoom(game, getPlayerPos(player), coordPlusOrientation(getPlayerPos(player), getPlayerOri(player)));
		if (currentRoom){
			looked = coordDiff(coordPlusOrientation(getPlayerPos(player), getPlayerOri(player)),currentRoom->ulc);
			object = gdsl_list_search(getRoomObjects(currentRoom), isObjectInCoord, &looked);
		}
	}

	if (!object){
		return ERR;
	}
	setPlayerHand(player, object->object);

	char *message=(char *)malloc(sizeof(char)*100);
	sprintf(message,"Has cogido %s",getObjectDescription(object->object));
	statusControllerDisplayMessage(message);
	free(message);

	gdsl_list_delete(getRoomObjects(currentRoom), pointerCmp, object);
	return OK;	
}

STATUS ologic_dropObj(Game *game){
	Player *player = getGamePlayer(game);
	Object *object = getPlayerHand(player);
	if (!object){
		return ERR;
	}
	Room *currentRoom = getGameCurrentRoom(game);
	Coord looked = coordPlusOrientation(getPlayerPos(player), getPlayerOri(player));
	if (doesCoordColide(game, getPlayerPos(player), looked) == YES){
		return ERR;
	}
	setPlayerHand(player, NOTHING);
	
	char *message=(char *)malloc(sizeof(char)*100);
	sprintf(message,"Has dejado %s",getObjectDescription(object));
	statusControllerDisplayMessage(message);
	free(message);

	RoomObject *newObject=(RoomObject *)malloc(sizeof(RoomObject));
	newObject->pos = coordDiff(looked, currentRoom->ulc);
	newObject->object=object;
	gdsl_list_insert_head(getRoomObjects(currentRoom), newObject);
	return OK;

}


void ologic_createRandomObjectInPosition(Game *game, gdsl_list_t objects, Coord pos){
	
	Room *bulletRoom=getRoomFromCoord(pos,&(getGameCurrentStage(game)->map));
	Object *object=gdsl_list_get_random(objects);	
	if(object->isWeapon==TRUE)
		return;
	logToFile("\n%s\n",object->description);
	RoomObject *roomObj=(RoomObject *)malloc(sizeof(RoomObject));
	roomObj->object=object;
	roomObj->pos=coordDiff(pos,bulletRoom->ulc);
	logToFile("\nCreando un objeto aleatorio en la posicion (%d,%d)\n",pos.x,pos.y);

	gdsl_list_insert_head(getRoomObjects(bulletRoom),roomObj);
}
