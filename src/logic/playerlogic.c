#include "playerlogic.h"

STATUS plogic_shoot(Game *game){
	Orientation ori;
	ori=getPlayerOri(getGamePlayer(game));
	if(!getPlayerWeapon(getGamePlayer(game)))
		return ERR;
	if(!getWeaponAmmo(getObjectWeapon(getPlayerWeapon(getGamePlayer(game)))))
		return ERR;
	gdsl_list_insert_head(getGameBullets(game),createBullet(ori, getPlayerPos(getGamePlayer(game))));
	updateWeaponAmmo(getObjectWeapon(getPlayerWeapon(getGamePlayer(game))), -1);
	if(!getWeaponAmmo(getObjectWeapon(getPlayerWeapon(getGamePlayer(game)))))
		statusControllerDisplayMessage("Te has quedado sin municion");
	return OK;
}

LIFE_STATUS plogic_damage(Game *game, int damage){
	
	updatePlayerLife(getGamePlayer(game),damage);
	Life life=getPlayerLife(getGamePlayer(game));
	if(life<=0)
		return DEAD;
	return ALIVE;
}

bool plogic_reload(Game *game,Object *object){
	
	if(!getPlayerWeapon(getGamePlayer(game)))
		return ERR;
	int ammo=getWeaponAmmo(getObjectWeapon(getPlayerWeapon(getGamePlayer(game))));
	/*comprobar la munición máxima del arma:
	  de momento solo hay un cargador infinito
	  IDEA: poner una recámara para guardar las balas del objeto e ir reduciendo esa recámara*/
	int amount=getObjectAmount(object);
	ammo+=amount;
	setWeaponAmmo(getObjectWeapon(getPlayerWeapon(getGamePlayer(game))),ammo);

	statusControllerDisplayMessage("Has usado %s",getObjectDescription(object));
	return OK;
}

void plogic_changeWeapon(Game *game, Object *obj){
	Player *player=getGamePlayer(game);
	Object *weapon=getPlayerWeapon(getGamePlayer(game));
	if(!weapon){
		setPlayerWeapon(player,obj);
		gdsl_list_remove(getPlayerBackpack(player),pointerCmp,obj);

		char *message=(char *)malloc(sizeof(char)*100);		
		sprintf(message,"Has equipado %s",getObjectDescription(obj));
		statusControllerDisplayMessage(message);
		free(message);
	}else{
		gdsl_list_insert_head(getPlayerBackpack(player),weapon);
		setPlayerWeapon(player,obj);

		char *message=(char *)malloc(sizeof(char)*100);		
		sprintf(message,"Has equipado %s",getObjectDescription(obj));
		statusControllerDisplayMessage(message);
		free(message);
		
		gdsl_list_remove(getPlayerBackpack(player),pointerCmp,obj);
	}
}
