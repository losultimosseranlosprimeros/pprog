#include "interface/superwindow.h"


int main(){

	#ifdef XCODE
		printf("Waiting for xCode to attach\n\n");
		int i;
		for (i = 0; i < 100000; ++i)
		{
			printf("\rprogress: %d %%",(int)(((double)i/100000) * 100));
			fflush(stdout);
		}
    #endif

	generalController *gc = generalControllerStart();
	freeGeneralController(gc);

	endwin();

	return 0;
}