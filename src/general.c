#include "general.h"
#include <stdarg.h>

int chstrlen(chtype * str){
	chtype * pstr;
	if(!str)return 0;
	for( pstr=str ; *pstr != 0 ; pstr++ );
	return pstr-str;

}

chtype * chstr(char *str){
	chtype *pch, *result = malloc((strlen(str)+1)*sizeof(chtype));
	for(pch=result;*str!=0;pch++,str++){
		*pch = (chtype)*str;
		*pch &= bitmask;
	}
	*pch = 0;
	return result;
}

void logToFile(const char *format,...){
	#if LOGGING
		FILE *f = fopen("log.txt", "a");
		va_list argptr;
	    va_start(argptr, format);
	    vfprintf(f,format,argptr);
	    va_end(argptr);
		fclose(f);
	#endif
}

char *strdump(const char *src){
	char *result = malloc((strlen(src)+1)*sizeof(char));
	if (!result) return NULL;
	strcpy(result, src);
	return result;
}

int coordcmp(int x, int y,Coord culc,Coord clrc){
	if(x-culc.x>=0 && x-clrc.x<=0 && y-culc.y>=0 && y-clrc.y<=0) 
		return YES;
	return NO;
}

int _applyPercentage(int num, int percent){
	return num+num*percent/100;
}

long int pointerCmp(const gdsl_element_t E, void *VALUE){
	if (E == VALUE){
		return FOUND;
	}
	return NOT_FOUND;
}

