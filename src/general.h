
#ifndef _generalheader
#define _generalheader

#include <string.h>
#include <stdlib.h>
#include "curses.h"
#include "../libs/jansson/src/jansson.h"
//#include <jansson.h>
#include <time.h>
#include <unistd.h>

#include "common/gdsl_improvement.h"

#define bitmask 255
#define usecsInSec 1000000

#define YES 1
#define NO 0

#define FOUND 0
#define NOT_FOUND 1

#define LOGGING YES

#define NOTHING NULL

typedef int Life;
#define MAX_LIFE 100

int chstrlen (chtype *str);

chtype *chstr (char *str);

typedef int (*cmd_func) (void *, void *, char *, char *);

void logToFile (const char *format, ...);

char *strdump (const char *src);

typedef int STATUS;

typedef enum {RIGHT=0, DOWN=1, LEFT=2, UP=3} DIRECTION, Orientation;

typedef enum {DEAD, ALIVE} LIFE_STATUS;

typedef enum { MODAL_MAP, MODAL_DEFAULTMENU,MODAL_OBJECTSMENU, MODAL_COMMAND, MODAL_TEXT, MODAL_STATUS }MODAL_TYPE;

long int pointerCmp (const gdsl_element_t E, void *VALUE);

typedef struct{
	short minutes,seconds;
}Runtime;

#define createTimeOfDay(name) struct timeval name; gettimeofday(& name, NULL)
#define timeValOffset(first,last) (last.tv_usec - first.tv_usec + usecsInSec*(last.tv_sec - first.tv_sec))

#include "common/coord.h"

#endif
