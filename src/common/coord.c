#include "coord.h"
#include "../general.h"
#include <math.h>

Coord createCoord (int x, int y) {
    Coord new;
    new.x = x;
    new.y = y;
    return new;
}

Coord coordDiff (Coord first, Coord second) {
    return createCoord (first.x - second.x, first.y - second.y);
}

Coord coordSum (Coord first, Coord second) {
    return createCoord (first.x + second.x, first.y + second.y);
}

Coord coordPlusOrientation (Coord first, Orientation ori) {
    Coord a = createCoord (0, 0);
    switch (ori) {
        case UP:
            a.y = -1;
            break;
        case DOWN:
            a.y = 1;
            break;
        case LEFT:
            a.x = -1;
            break;
        default:
        case RIGHT:
            a.x = 1;
            break;
    }

    return coordSum(first, a);
}

int coordAproxModulus(Coord c){
    return (int)sqrt((( c.x*c.x ) + (c.y * c.y)));
}

Square createSquare (Coord ulc, Coord lrc) {
    Square new;
    new.ulc = ulc;
    new.lrc = lrc;
    return new;
}

Coord squareLowerLeft (Square sq) {
    return createCoord (sq.ulc.x, sq.lrc.y);
}

Coord squareUpperRight (Square sq) {
    return createCoord (sq.lrc.x, sq.ulc.y);
}

Square moveSquareUlc (Square sq, Coord newUlc) {
    Coord size = coordDiff (sq.lrc, sq.ulc);
    sq.ulc = newUlc;
    sq.lrc = coordSum (newUlc, size);
    return sq;
}

Coord squareSize (Square sq) {
    return coordDiff (sq.lrc, sq.ulc);
}

Coord realSquareSize(Square sq){
    return coordSum(squareSize(sq), createCoord(1, 1));
}

Coord squareCenter(Square sq){
    return createCoord ((sq.ulc.x + sq.lrc.x) / 2, (sq.ulc.y + sq.lrc.y) / 2);
}

int squaresOverlap (Square sq1, Square sq2) {

    return (! (sq1.lrc.y < sq2.ulc.y || sq1.ulc.y > sq2.lrc.y || sq1.lrc.x < sq2.ulc.x || sq1.ulc.x > sq2.lrc.x ) );

}

int isCoordBetweenCords (Coord inside, Coord c1, Coord c2) {

    Coord aux = coordDiff (inside, c1);
    if (aux.y < 0 || aux.x < 0)
        return NO;
    aux = coordDiff (c2, inside);
    if (aux.y < 0 || aux.x < 0)
        return NO;
    return YES;

}

int isPointBetweenCoords (int x, int y, Coord c1, Coord c2) {
    return isCoordBetweenCords (createCoord (x, y), c1, c2);
}

int isCoordInPerimeter (Coord c, Square perimeter) {
    if (c.x == perimeter.ulc.x || c.x == perimeter.lrc.x || c.y == perimeter.ulc.y || c.y == perimeter.lrc.y)
        return YES;
    return NO;
}

int areCoordsEqual (Coord a, Coord b) {
    if (a.x == b.x && a.y == b.y)
        return YES;
    return NO;
}

Coord coordFromJson (json_t *jobject) {

    Coord c;
    json_t *number;
    number = json_object_get (jobject, "x");
    c.x = json_number_value (number);
    number = json_object_get (jobject, "y");
    c.y = json_number_value (number);
    return c;

}

Coord randomCoordInsideSquare(Square sq, bool includeBorders){

    Coord max = squareSize(sq);
    Coord res = {0,0};

    if (includeBorders == NO){
        max.x--;
        max.y--;
        res.x++;
        res.y++;
    }else{
        max.x++;
        max.y++;
    }

    if(!max.y || !max.x){
        return sq.ulc;
    }

    res.x += (rand() % max.x);
    res.y += (rand() % max.y);

    return coordSum(res, sq.ulc);
}
