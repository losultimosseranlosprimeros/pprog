
#include "gdsl_improvement.h"
#include <stdlib.h>
#include <string.h>



/* PRIVATE STRUCTURES AND FUNCTIONS */

typedef struct {

    void *data;
    gdsl_list_t new;
    gdsl_filter_func_t filter;
    bool inverse;

} map_filter_helper;

static gdsl_list_t _gdsl_list_filtered_copy (gdsl_list_t list, gdsl_filter_func_t func, void *data, bool inverse);

int _map_to_filter (gdsl_element_t elem, gdsl_location_t location, void *data);

/*************************************/


gdsl_element_t gdsl_list_get_random (gdsl_list_t list) {
    if (gdsl_list_get_size(list) == 0)return NULL;
    ulong pos = ( rand() % gdsl_list_get_size (list) ) + 1;
    return gdsl_list_search_by_position (list, pos);
}


gdsl_list_t gdsl_list_filtered_copy (gdsl_list_t list, gdsl_filter_func_t func, void *data) {
    return _gdsl_list_filtered_copy (list, func, data, FALSE);
}

gdsl_list_t gdsl_list_inverse_filtered_copy (gdsl_list_t list, gdsl_filter_func_t func, void *data) {
    return _gdsl_list_filtered_copy (list, func, data, TRUE);

}

static gdsl_list_t _gdsl_list_filtered_copy (gdsl_list_t list, gdsl_filter_func_t func, void *data, bool inverse) {

    /* Create new name */
    char *string = malloc ( (10 + strlen (gdsl_list_get_name (list) ) ) * sizeof (char) );
    strcpy (string, gdsl_list_get_name (list) );
    strcpy (string + strlen (string), "_filtered");

    /* Create list */
    gdsl_list_t result = gdsl_list_alloc (string, NULL, NULL);
    free (string);

    /* Create user data */
    map_filter_helper helper;
    helper.data = data;
    helper.new = result;
    helper.filter = func;
    helper.inverse = inverse;

    /* fill new list */
    gdsl_list_map_forward (list, _map_to_filter, &helper);

    return result;
}

int _map_to_filter (gdsl_element_t elem, gdsl_location_t location, void *data) {

    map_filter_helper *helper = data;
    gdsl_filter_func_t filter_func = helper->filter;

    /* This is a bit hacky */
    if ( filter_func (elem, helper->data) + helper->inverse == 1 ) {
        gdsl_list_insert_head (helper->new, elem);
    }

    return GDSL_MAP_CONT;

}