#ifndef _coordheader
#define _coordheader

#include "../general.h"

typedef struct{
	int x,y;
}Coord;

typedef struct{
	Coord ulc,lrc;
}Square;

typedef struct{
	char **image;
	Square bounds;
}Image;

Coord createCoord (int x, int y);

Coord coordDiff (Coord first, Coord second);

Coord coordSum (Coord first, Coord second);

Coord coordPlusOrientation (Coord first, Orientation ori);

int coordAproxModulus(Coord c);

Square createSquare (Coord ulc, Coord lrc);

Coord squareLowerLeft (Square sq);

Coord squareUpperRight (Square sq);

/* Conserva el tamaño */
Square moveSquareUlc(Square sq, Coord newUlc);

Coord squareSize(Square sq);

Coord realSquareSize(Square sq);

Coord squareCenter(Square sq);

int squaresOverlap (Square sq1, Square sq2);

int isCoordBetweenCords (Coord inside, Coord c1, Coord c2);

int isPointBetweenCoords (int x, int y, Coord c1, Coord c2);

int isCoordInPerimeter (Coord c, Square perimeter);

int areCoordsEqual (Coord a, Coord b);

Coord coordFromJson(json_t *jobject);

Coord randomCoordInsideSquare(Square sq, bool includeBorders);

#endif
