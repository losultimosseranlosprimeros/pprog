
//#include <gdsl.h>
#include "../../libs/gdsl/src/gdsl.h"

#define GDSL_INCLUDE 1
#define GDSL_DONT_INCLUDE 0

typedef int (*gdsl_filter_func_t) (gdsl_element_t element, void *data);

gdsl_list_t gdsl_list_filtered_copy (gdsl_list_t list, gdsl_filter_func_t func, void *data);

gdsl_list_t gdsl_list_inverse_filtered_copy (gdsl_list_t list, gdsl_filter_func_t func, void *data);

gdsl_element_t gdsl_list_get_random (gdsl_list_t list);
