#include "object.h"

/* funciones privadas */

static long int doesObjectHaveId(const gdsl_element_t object, void*id);

/***********************/


Object *iniObjectj(json_t *jobject, gdsl_list_t weapons){
		
	Object *new = malloc(sizeof(Object));

	json_t *jdata = json_object_get(jobject, "id");
	new->id = json_integer_value(jdata);
	jdata = json_object_get(jobject, "amount");
	new->amount = json_integer_value(jdata);
	jdata = json_object_get(jobject, "description");
	
	new->description = strdump(json_string_value(jdata));
	//logToFile("\n%s",new->description);


	jdata = json_object_get(jobject, "equipable");
	new->equipable = json_is_true(jdata) ? 1 : 0;
	jdata = json_object_get(jobject, "available");
	new->available = json_is_true(jdata) ? 1 : 0;
	jdata = json_object_get(jobject, "isWeapon");
	new->isWeapon = json_is_true(jdata) ? 1 : 0;
	jdata = json_object_get(jobject, "blocks");
	new->blocks = json_is_true(jdata) ? 1 : 0;
	jdata = json_object_get(jobject, "cmd_equip");
	new->cmd_equip = strdump(json_string_value(jdata));
	jdata = json_object_get(jobject, "character");
	new->character = json_string_value(jdata)[0];

	if(new->isWeapon==TRUE)
		new->weapon = searchWeaponInList(weapons, new->description);
	else
		new->weapon=NULL;
	//logToFile("\n%s",new->weapon->name);
	
	return new;
}


int getObjectId(Object *obj){
	return obj->id;
}

int getObjectAmount(Object *obj){
	return obj->amount;
}

void updateObjectAmount(Object *obj,int incr){
	obj->amount+=incr;
}

void setObjectAmount(Object *obj,int amount){
	obj->amount=amount;
}

char *getObjectDescription(Object *obj){
	return obj->description;
}

bool isObjectAvailable(Object *obj){
	return obj->available;
}

bool isObjectEquipable(Object *obj){
	return obj->equipable;
}

bool isObjectWeapon(Object *obj){
	return obj->isWeapon;
}

char *getObjectCmd(Object *obj){
	return obj->cmd_equip;
}

char getObjectCharacter(Object *obj){
	return obj->character;
}

Weapon *getObjectWeapon(Object *obj){
	return obj->weapon;
}

void freeObject(void *objf){
	Object *obj = objf;
	free(obj->description);
	free(obj->cmd_equip);
	free(obj);
}

Object * searchObjectInList(gdsl_list_t list, int id){
	return gdsl_list_search(list, doesObjectHaveId, &id);
}

static long int doesObjectHaveId(const gdsl_element_t object, void*id){
	if (((Object *)object)->id == *(int *)id) {
		return FOUND;
	}
	return NOT_FOUND;
}



