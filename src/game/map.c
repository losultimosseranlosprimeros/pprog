#include "map.h"

typedef struct{
	Coord pos;
	Room *room1;
	Room *room2;
} _getRoomFromCoordHelper;

int _getRoomFromCoord (const gdsl_element_t elem, gdsl_location_t location, void *data);


void iniMap(Map *map, json_t *jobject, gdsl_list_t objects){

	int i;
	Room *room;
	json_t *jcoord, *jrooms, *jroom;

	jcoord = json_object_get(jobject, "posI");
	map->posI = coordFromJson(jcoord);
	jcoord = json_object_get(jobject, "posF");
	map->posF = coordFromJson(jcoord);
	jrooms = json_object_get(jobject, "rooms");

	map->rooms = gdsl_list_alloc("rooms", NULL, freeRoom);

	gdsl_list_t voids = gdsl_list_alloc("voids", NULL, free);

	for(i=0; i<json_array_size(jrooms); i++){
		jroom = json_array_get(jrooms, i);
		room = iniRoom(jroom,voids, objects);
		gdsl_list_insert_head(map->rooms, room);
	}

	for (i = 1; i <= gdsl_list_get_size(voids); ++i){
		RoomVoid *rv = gdsl_list_search_by_position(voids, i);
		Room **aux = getRoomsFromVoidCoord(rv->pos, map);
		if (aux[0]==aux[1])
			logToFile("Equal Rooms: %d %d",aux[0]->ulc.x,aux[0]->ulc.y);
		
		roomInsertVoid(aux[0], rv->pos, aux[1]);
		roomInsertVoid(aux[1], rv->pos, aux[0]);
		free(aux);
	}
	gdsl_list_free(voids);
}


gdsl_list_t getMapRooms(Map *map){
	return map->rooms;
}

void insertMapRoom(Map *map, Room *room){
	gdsl_list_insert_head (map->rooms, room);
}

void setMapPosI(Map *map, Coord posI){
	map->posI=posI;
}

void setMapPosF(Map *map, Coord posF){
	map->posF=posF;
}

Coord getMapPosI(Map *map){
	return map->posI;
}

Coord getMapPosF(Map *map){
	return map->posF;
}

Room *getRoomFromCoord (Coord pos, Map *map){
	_getRoomFromCoordHelper helper;
	helper.pos=pos;
	helper.room1=NULL;
	helper.room2=NULL;
	gdsl_list_map_forward (map->rooms,_getRoomFromCoord,&helper);
	return helper.room1;
}

Room **getRoomsFromVoidCoord (Coord pos, Map *map){
	_getRoomFromCoordHelper helper;
	helper.pos=pos;
	helper.room1=NULL;
	helper.room2=NULL;
	gdsl_list_map_forward (map->rooms,_getRoomFromCoord,&helper);
	Room **aux=calloc(2, sizeof( Room * ));
	aux[0]=helper.room1;
	aux[1]=helper.room2;
	return aux;
}

int _getRoomFromCoord (const gdsl_element_t elem, gdsl_location_t location, void *data){
	Room *room=elem;
	_getRoomFromCoordHelper *helper;
	helper = (_getRoomFromCoordHelper *) data;
	if(isCoordBetweenCords(helper->pos,room->ulc,room->lrc)){
		if (!helper->room1){
			helper->room1 = room;
		}else if (!helper->room2){
			helper->room2 = room;
		}
	}
	return GDSL_MAP_CONT;
}


void freeMap(Map *map){
	gdsl_list_free(map->rooms);
}