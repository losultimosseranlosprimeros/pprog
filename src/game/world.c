#include "world.h"


World *iniWorld( json_t *json ) {

    Stage *st;
    int i;
    json_t *jstages, *jstage, *jobjects, *jobject, *jweapons, *jweapon;
    World *world = malloc (sizeof (World) );

    world->objects = gdsl_list_alloc ("objects", NULL, freeObject);

    world->weapons = gdsl_list_alloc ("weapons", NULL, free);

    jweapons = json_object_get(json, "weapons");
    for (i = 0; i < json_array_size(jweapons); ++i){
        jweapon = json_array_get(jweapons, i);
        gdsl_list_insert_head (world->weapons, iniWeapon(jweapon));
    }

    jobjects = json_object_get(json, "objects");
    for (i = 0; i < json_array_size(jobjects); ++i){
        jobject = json_array_get(jobjects, i);
        gdsl_list_insert_head (world->objects, iniObjectj(jobject,world->weapons));
    }

    world->stages = gdsl_list_alloc ("stages", NULL, freeStage);

    jstages = json_object_get(json, "stages");
    for (i = 0; i < json_array_size(jstages); ++i) {
        jstage = json_array_get(jstages, i);
        st = iniStage(jstage, world->objects);
        gdsl_list_insert_head (world->stages, st);
    }

    return world;

}


gdsl_list_t getWorldObjects (World *world) {
    return world->objects;
}

gdsl_list_t getWorldStages (World *world) {
    return world->stages;
}

gdsl_list_t getWorldWeapons (World *world) {
    return world->weapons;
}

void freeWorld(World *world){
    gdsl_list_free(world->weapons);
    gdsl_list_free(world->objects);
    gdsl_list_free(world->stages);
    free(world);
}