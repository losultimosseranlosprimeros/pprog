#ifndef _mapheader
#define _mapheader

#include "../general.h"
#include "room.h"

typedef struct{

	Coord posI, posF;
	gdsl_list_t rooms;

}Map;


void iniMap(Map *map, json_t *jobject, gdsl_list_t objects);

gdsl_list_t getMapRooms(Map *map);

void insertMapRoom(Map *map, Room *room);

void setMapPosI(Map *map, Coord posI);

void setMapPosF(Map *map, Coord posF);

Coord getMapPosI(Map *map);

Coord getMapPosF(Map *map);

Room *getRoomFromCoord (Coord pos, Map *map);

Room **getRoomsFromVoidCoord (Coord pos, Map *map);

void freeMap(Map *map);

#endif
