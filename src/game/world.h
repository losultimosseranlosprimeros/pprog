
#ifndef _worldheader
#define _worldheader

#include "../general.h"
#include "object.h"
#include "stage.h"
#include "player.h"


typedef struct{

	gdsl_list_t stages,objects,weapons;

}World;

World * iniWorld( json_t *json );

gdsl_list_t getWorldObjects(World *world);

gdsl_list_t getWorldStages(World *world);

gdsl_list_t getWorldWeapons(World *world);

STATUS worldAddStage(World * world, Stage *stage);

void freeWorld(World *world);



#endif
