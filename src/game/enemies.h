#ifndef _enemiesheader
#define _enemiesheader

#include "../general.h"


typedef struct{
	Coord pos;
	Orientation ori;
	Life life;
	int speed;
}Enemie;

Enemie *iniEnemie(Coord pos, Orientation ori, Life life, int speed);

Coord getEnemiePos(Enemie *enemie);

Orientation getEnemieOri(Enemie *enemie);

Life getEnemieLife(Enemie *enemie);

int getEnemieSpeed(Enemie *enemie);

void setEnemiePos(Enemie *enemie, Coord pos);

void setEnemieOri(Enemie *enemie, Orientation ori);

void setEnemieLife(Enemie *enemie, Life life);

void updateEnemieLife(Enemie *enemie, int incr);

void freeEnemie(Enemie *enemie);

long int isEnemyInCoord(gdsl_element_t enemy, void *coord);

int enemiesInSquareFilter (const gdsl_element_t element, void *data);

#endif