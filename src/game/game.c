
#include "game.h"
#include "../logic/fightlogic/flogicia.h"

Game *iniGame(Room *currentRoom, Stage *currentStage,World *world){
	Game *game=(Game *)malloc(sizeof(Game));
	game->currentRoom=currentRoom;
	game->currentStage=currentStage;
    game->player = playerIni(getMapPosI(getStageMap(currentStage)), DOWN, MAX_LIFE);

    game->enemieList=gdsl_list_alloc("enemies",NULL,free);
    game->bulletList=gdsl_list_alloc("bullets",NULL,free);
    gettimeofday(&game->prevTime, NULL);
    game->runtime.minutes = game->runtime.seconds = 0;
    game->paused = NO;
    game->waveNumber = 0;
    game->ia = flogicia_init(game);
    flogicia_update_weights(game->ia,game);
    return game;
}

void pauseGame(Game *game){
    if (game->paused == YES) return;
    game->paused = YES;
    gettimeofday(&game->timePaused, NULL);
}

void resumeGame(Game *game){
    if (game->paused == NO) return;
    game->paused = NO;
    int offset = timeValOffset(game->prevTime,game->timePaused);
    gettimeofday(&game->prevTime, NULL);
    int secs = offset/usecsInSec;
    int usecs = offset%usecsInSec;
    game->prevTime.tv_sec-=secs;
    game->prevTime.tv_usec-=usecs;
}

bool isGamePaused(Game *game){
    return game->paused;
}

GAME_STATE updateGameState(Game *game){
    if (getPlayerLife(game->player)<= 0){
        return GAME_FINISHED;
    }
    if (game->paused == YES){
        return GAME_PAUSED;
    }
    return GAME_RUNNING;
}

int getGameWaveNumber(Game *game){
    return game->waveNumber;
}

int incrementWaveNumber(Game *game){
    return ++game->waveNumber;
}

Runtime getGameRuntime(Game *game){
    if (game->paused == YES) return game->runtime;
    createTimeOfDay(tv);
    int seconds;
    if ((seconds = tv.tv_sec - game->prevTime.tv_sec)){
        game->runtime.seconds += seconds;
        if (game->runtime.seconds >= 60){
            game->runtime.minutes += game->runtime.seconds/60;
            game->runtime.seconds %= 60;
        }
        game->prevTime = tv;
    }
    return game->runtime;
}

Bullet *createBullet(DIRECTION dir, Coord pos){
	Bullet *bullet=malloc(sizeof(Bullet));
	bullet->pos=pos;
	bullet->dir=dir;
	return bullet;
}

Room *getGameCurrentRoom(Game *game){
	return game->currentRoom;
}

Stage *getGameCurrentStage(Game *game){
	return game->currentStage;
}

Player *getGamePlayer(Game *game){
	return game->player;
}

gdsl_list_t getGameEnemies(Game *game){
    return game->enemieList;
}

gdsl_list_t getGameBullets(Game *game){
    return game->bulletList;
}

IA *getGameIA(Game *game){
    return game->ia;
}

Square getGameVisibleSquare(Game *game){
    return game->visibleSquare;
}

void setGameCurrentRoom(Room *room,Game *game){
	game->currentRoom=room;
}

void setGameCurrentStage(Stage *stage,Game *game){
	game->currentStage=stage;
}

Room *otherRoom(Game *game,Coord curr, Coord next){

    Room *currentRoom = getGameCurrentRoom(game);
    Coord relative = coordDiff(curr, currentRoom->ulc);

    RoomVoid *rv = gdsl_list_search (getRoomVoids (currentRoom), isVoidInCoord, &relative);
    if ( rv ){
        logToFile("found ");
        return rv->adyacent;
    }

    relative = coordDiff(next, currentRoom->ulc);
    rv = gdsl_list_search (getRoomVoids(currentRoom), isVoidInCoord, &relative);

    if (rv){
        logToFile("found ");
        return rv->adyacent;
    }

    return NULL;

}

void freeGame(Game *game){
    gdsl_list_free(game->enemieList);
    gdsl_list_free(game->bulletList);
    freePlayer(game->player);
    free(game);
}

Coord getBulletPos(Bullet *bullet){
    return bullet->pos;
}

DIRECTION getBulletDir(Bullet *bullet){
    return bullet->dir;
}

void setBulletDir(Bullet *bullet,DIRECTION dir){
    bullet->dir=dir;
}

void setBulletPos(Bullet *bullet, Coord pos){
    bullet->pos=pos;
}

long int isBulletInCoord(gdsl_element_t bullet, void *coord){
    Bullet *checked = bullet;
    if (areCoordsEqual(checked->pos, *(Coord *)coord)==YES){
        return FOUND;
    }
    return NOT_FOUND;
}