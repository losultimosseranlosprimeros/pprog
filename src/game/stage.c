
#include "stage.h"

Stage *iniStage(json_t *jobject, gdsl_list_t objects) {
	json_t *map;
	Stage *newStage = malloc(sizeof(Stage));

	map = json_object_get(jobject, "map");

	iniMap(&newStage->map, map, objects);

	return newStage;
}

void setStageMap(Stage *stage, Map *map){
	stage->map=*map;	
}

Map *getStageMap(Stage *stage){
	return &stage->map;
}

void freeStage(void *stagef){
	Stage *stage = stagef;
	freeMap(&stage->map);
	free(stage);
}