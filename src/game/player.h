#ifndef _playerheader
#define _playerheader

#include "../general.h"
#include "weapon.h"
#include "object.h"

typedef struct{
	Coord pos;
	Orientation ori;
	Life life;
	Object *weapon;
	gdsl_list_t backpack;
	Object *hand;
}Player;

typedef struct{
	Object *obj;
	int amount;
}inventaryObject;

Player *playerIni(Coord pos, Orientation ori, Life life);

Coord getPlayerPos(Player *player);

Orientation getPlayerOri(Player *player);

Life getPlayerLife(Player *player);

Object *getPlayerWeapon(Player *player);

gdsl_list_t getPlayerBackpack(Player *player);

void setPlayerPos(Coord pos, Player *player);

void setPlayerOri(Orientation ori, Player *player);

void setPlayerLife(Life life, Player *player);

void updatePlayerLife(Player *player, int incr);

void setPlayerWeapon(Player *player,Object *weapon);

void setPlayerHand(Player *player, Object *object);

Object *getPlayerHand(Player *player);

void freePlayer(Player *player);

int getInventaryObjectAmount(inventaryObject *iObj);

void setInventaryObjectAmount(inventaryObject *iObj, int amount);

Object *getObjectInventaryObject(inventaryObject *iObj);

void setObjectInventaryObjectAmount(inventaryObject *iObj, Object *obj);

#endif
