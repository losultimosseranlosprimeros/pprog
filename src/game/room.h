#ifndef _roomheader
#define _roomheader

#include "../general.h"
#include "object.h"

typedef struct {

    Object *object;
    Coord pos;

} RoomObject;

typedef struct {
	
    Coord ulc, lrc;
    bool zombiable;
    gdsl_list_t objects, roomVoids;

} Room;


typedef struct {

    Coord pos;
    Room *adyacent;

} RoomVoid;

Room *iniRoom ( json_t *jobject, gdsl_list_t voids, gdsl_list_t objects );

gdsl_list_t getRoomObjects (Room *room);

gdsl_list_t getRoomVoids (Room *room);

Square getRoomCoords(Room *room);

void setRoomCoords(Room *room, Square *coords);

void roomInsertObject (Room *room, int id, Coord pos, gdsl_list_t objects);

void roomInsertVoid (Room *room, Coord pos, Room *next);
void roomLoadVoid (Room *room, Coord pos, gdsl_list_t voids);

void roomInsertVoidWall (Room *room, Coord c1, Coord c2, Room *next);
void roomLoadVoidWall (Room *room, Coord c1, Coord c2, gdsl_list_t voids);

void freeRoom ( void *room );

long int isObjectInCoord (const gdsl_element_t object, void *coord);
long int isVoidInCoord (const gdsl_element_t roomVoid, void *coord);
int visibleRoomFilter (const gdsl_element_t element, void *data);

#endif
