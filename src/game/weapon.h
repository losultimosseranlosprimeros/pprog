#ifndef _weaponheader
#define _weaponheader

#include "../general.h"


typedef struct{
	char *name;
	int ammo;
	int damage;
	int range;
}Weapon;

Weapon *iniWeapon(json_t *json);

void updateWeaponAmmo(Weapon *weapon, int incr);

void setWeaponDamage(Weapon *weapon, int damage);

void setWeaponAmmo(Weapon *weapon, int ammo);

void setWeaponRange(Weapon *weapon, int range);

void setWeaponName(Weapon *weapon, char *name);

int getWeaponAmmo(Weapon *weapon);

int getWeaponDamage(Weapon *weapon);

int getWeaponRange(Weapon *weapon);

char *getWeaponName(Weapon *weapon);

Weapon * searchWeaponInList(gdsl_list_t weapons, char *name);

int getWeaponDamageFromDistance(Weapon *weapon, int distance);

void freeWeapon(Weapon *weapon);

#endif
