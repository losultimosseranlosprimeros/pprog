#ifndef _objectheader
#define _objectheader

#include "../general.h"
#include "weapon.h"

typedef struct {

	int id;
	int amount;
	char *description;
	Weapon *weapon;
	bool available, equipable, isWeapon, blocks;
	char *cmd_equip;
	char character;

}Object;

Object *iniObjectj(json_t *jobject,gdsl_list_t weapons);

int getObjectId(Object *obj);

int getObjectAmount(Object *obj);

Weapon *getObjectWeapon(Object *obj);

void setObjectAmount(Object *obj,int amount);

char *getObjectDescription(Object *obj);

bool isObjectAvailable(Object *obj);

bool isObjectEquipable(Object *obj);

bool isObjectWeapon(Object *obj);

char *getObjectCmd(Object *obj);

char getObjectCharacter(Object *obj);

void freeObject(void *obj);

Object *searchObjectInList(gdsl_list_t list, int id);


#endif
