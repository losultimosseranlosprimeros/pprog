#include "../general.h"
#include "world.h"

typedef struct{
	World *world;
	FILE *f;
}Diff;

typedef enum{CHANGE_PLAYER_POSITION,CHANGE_PLAYER_LIFE,CHANGE_PLAYER_EQUIPMENT,CHANGE_WORLD_STAGE,CHANGE_WORLD_OBJECT,CHANGE_WORLD_DOOR}CHANGE_TYPE;

typedef struct{
	CHANGE_TYPE type;
	void *data;
}Change;

Diff *loadGame(char *file);

int saveGame(Diff *df);

int applyChange(Diff *df, Change ch);