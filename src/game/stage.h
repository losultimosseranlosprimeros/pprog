#ifndef _stageheader
#define _stageheader

#include "map.h"

typedef struct {
	Map map;
}Stage;


Stage *iniStage(json_t *jobject, gdsl_list_t objects);

void setStageMap(Stage *stage, Map *map);

Map *getStageMap(Stage *stage);

void freeStage(void *stage);

#endif
