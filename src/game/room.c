#include "room.h"
#include "object.h"



Room *iniRoom ( json_t *jobject, gdsl_list_t voids, gdsl_list_t objects ) {

    int i;
    Coord auxCoord, auxCoord2;
    json_t *jdata, *jcoord, *jroomObjects, *jroomObject, *jvoids, *jvoid, *jvoidArray, *jroomObjectId;
    Room *new = malloc (sizeof (Room) );

    jcoord = json_object_get (jobject, "ulc");
    new->ulc = coordFromJson (jcoord);
    jcoord = json_object_get (jobject, "lrc");
    new->lrc = coordFromJson (jcoord);
    jdata = json_object_get(jobject, "zombiable");
    new->zombiable = json_is_true(jdata) ? 1 : 0;

    new->objects = gdsl_list_alloc ("objects", NULL, free);
    new->roomVoids = gdsl_list_alloc ("voids", NULL, free);

    jvoids = json_object_get (jobject, "voids");
    jvoidArray = json_object_get (jvoids, "singles");
	jroomObjects = json_object_get (jobject, "objects");
	
	
	for(i = 0; i < json_array_size (jroomObjects); ++i) {
        jroomObject = json_array_get(jroomObjects, i);
		jroomObjectId = json_object_get(jroomObject, "id");
		
		jcoord=json_object_get(jroomObject, "pos");
        auxCoord = coordFromJson(jcoord);
        roomInsertObject(new,json_number_value(jroomObjectId),auxCoord,objects);
    }


    for (i = 0; i < json_array_size (jvoidArray); ++i) {
        jvoid = json_array_get(jvoidArray, i);
        auxCoord = coordFromJson(jvoid);
        roomLoadVoid(new, auxCoord, voids);
    }

    jvoidArray = json_object_get (jvoids, "walls");

    for (i = 0; i < json_array_size (jvoidArray); ++i) {
        jvoid = json_array_get(jvoidArray, i);
        jcoord = json_object_get(jvoid, "origin");
        auxCoord = coordFromJson(jcoord);
        jcoord = json_object_get(jvoid, "end");
        auxCoord2 = coordFromJson(jcoord);
        roomLoadVoidWall(new,auxCoord, auxCoord2, voids);
    }

    return new;

}

gdsl_list_t getRoomObjects (Room *room) {
    return room->objects;
}

gdsl_list_t getRoomVoids (Room *room) {
    return room->roomVoids;
}

void roomInsertObject (Room *room, int id, Coord pos, gdsl_list_t objects) {
    RoomObject *newObject = malloc ( sizeof (RoomObject) );
    newObject->object = searchObjectInList(objects, id);
    newObject->pos = pos;
    gdsl_list_insert_head (room->objects, newObject);
}

void roomInsertVoid (Room *room, Coord pos, Room *next) {
    if (!room) return;
    RoomVoid *newVoid = malloc ( sizeof (RoomVoid) );
    newVoid->pos = coordDiff(pos, room->ulc);
    newVoid->adyacent = next;
    gdsl_list_insert_head (room->roomVoids, newVoid);
}

void roomLoadVoid (Room *room, Coord pos, gdsl_list_t voids){
    RoomVoid *newVoid = malloc ( sizeof (RoomVoid) );
    newVoid->pos = coordSum(room->ulc, pos);
    gdsl_list_insert_head(voids, newVoid);
}

void roomInsertVoidWall (Room *room, Coord c1, Coord c2, Room *next) {
    int i;
    if (c1.x - c2.x != 0) {
        for (i = c1.x; i <= c2.x ; ++i)
            roomInsertVoid (room, createCoord (i, c1.y), next );

    } else if (c1.y - c2.y != 0) {
        for (i = c1.y; i <= c2.y ; ++i)
            roomInsertVoid (room, createCoord (c1.x, i), next );

    }
}

void roomLoadVoidWall (Room *room, Coord c1, Coord c2, gdsl_list_t voids) {
    int i;
    if (c1.x - c2.x != 0) {
        for (i = c1.x; i <= c2.x ; ++i)
            roomLoadVoid (room, createCoord (i, c1.y), voids );

    } else if (c1.y - c2.y != 0) {
        for (i = c1.y; i <= c2.y ; ++i)
            roomLoadVoid (room, createCoord (c1.x, i), voids );

    }
}

Square getRoomCoords(Room *room){

	return createSquare(room->ulc,room->lrc);

}

long int isObjectInCoord (const gdsl_element_t object, void *coord) {

    RoomObject *ro = object;
    Coord c = * ( (Coord *) coord);

    if ( areCoordsEqual (c, ro->pos) )
        return FOUND;
    return NOT_FOUND;

}

long int isVoidInCoord (const gdsl_element_t roomVoid, void *coord) {

    RoomVoid *rv = roomVoid;
    Coord c = * ( (Coord *) coord);

    if ( areCoordsEqual (c, rv->pos) )
        return FOUND;
    return NOT_FOUND;

}

int visibleRoomFilter (const gdsl_element_t element, void *data) {
    Room *room = element;
    Square *helper;
    helper = data;

    if ( squaresOverlap(createSquare(helper->ulc, helper->lrc), createSquare(room->ulc, room->lrc))){
        return GDSL_INCLUDE;
    }

    return GDSL_DONT_INCLUDE;
}

void freeRoom(void *roomf){
    Room *room = (void *)roomf;

    gdsl_list_free(room->objects);
    gdsl_list_free(room->roomVoids);
    free(room);
}

