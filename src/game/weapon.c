
#include "weapon.h"

long int _doesWeaponHaveName(const gdsl_element_t weapon, void *name);

Weapon *iniWeapon(json_t *json){
	Weapon *weapon=(Weapon *)malloc(sizeof(Weapon));
	json_t *jdata;
	jdata=json_object_get(json, "name");
	weapon->name=strdup(json_string_value(jdata));
	jdata=json_object_get(json, "ammo");
	weapon->ammo=json_integer_value(jdata);
	jdata=json_object_get(json, "damage");
	weapon->damage=json_integer_value(jdata);
	jdata=json_object_get(json, "range");
	weapon->range=json_integer_value(jdata);
	return weapon;
}

void setWeaponName(Weapon *weapon, char *name){
	weapon->name=name;
}

void updateWeaponAmmo(Weapon *weapon, int incr){
	weapon->ammo+=incr;
}

void setWeaponAmmo(Weapon *weapon, int ammo){
	weapon->ammo=ammo;
}

void setWeaponDamage(Weapon *weapon, int damage){
	weapon->damage=damage;
}

void setWeaponRange(Weapon *weapon, int range){
	weapon->range=range;
}

char *getWeaponName(Weapon *weapon){
	return weapon->name;
}


int getWeaponAmmo(Weapon *weapon){
	return weapon->ammo;
}

int getWeaponDamage(Weapon *weapon){
	return weapon->damage;
}

int getWeaponRange(Weapon *weapon){
	return weapon->range;
}

void freeWeapon(Weapon *weapon){
	free(weapon->name);
	free(weapon);
}

int getWeaponDamageFromDistance(Weapon *weapon, int distance){
	return OK;
}

Weapon *searchWeaponInList(gdsl_list_t weapons, char *name){
	Weapon *weapon=gdsl_list_search(weapons,_doesWeaponHaveName,name);
	logToFile("%s\t%d\t%d",weapon->name,weapon->damage,weapon->ammo);
	return weapon;
}

long int _doesWeaponHaveName(const gdsl_element_t weapon, void * name){
	if (!strcmp(((Weapon *)weapon)->name, (char *)name)){
		return FOUND;
	}
	return NOT_FOUND;
}
