#include "enemies.h"

Enemie *iniEnemie(Coord pos, Orientation ori, Life life, int speed){
	Enemie *enemie=(Enemie *)malloc(sizeof(Enemie));
	enemie->pos=pos;
	enemie->ori=ori;
	enemie->life=life;
	enemie->speed=speed;
	return enemie;
}

Coord getEnemiePos(Enemie *enemie){
	return enemie->pos;
}

Orientation getEnemieOri(Enemie *enemie){
	return enemie->ori;
}

Life getEnemieLife(Enemie *enemie){
	return enemie->life;
}

int getEnemieSpeed(Enemie *enemie){
	return enemie->speed;
}

void setEnemiePos(Enemie *enemie, Coord pos){
	enemie->pos=pos;
}

void setEnemieOri(Enemie *enemie, Orientation ori){
	enemie->ori=ori;
}

void setEnemieLife(Enemie *enemie, Life life){
	enemie->life=life;
}

void updateEnemieLife(Enemie *enemie, int incr){
	enemie->life+=incr;
}

void freeEnemie(Enemie *enemie){
	free(enemie);
}

long int isEnemyInCoord(gdsl_element_t enemy, void *coord){
	Enemie *checked = enemy;
	if (areCoordsEqual(checked->pos, *(Coord *)coord)==YES){
		return FOUND;
	}
	return NOT_FOUND;
}


int enemiesInSquareFilter (const gdsl_element_t element, void *data){
	Enemie *enemie=(Enemie *)element;
	Square *perimeter=(Square *)data;
	if(isCoordInPerimeter(getEnemiePos(enemie),*perimeter)){
		return GDSL_INCLUDE;
	}
	return GDSL_DONT_INCLUDE;
}

