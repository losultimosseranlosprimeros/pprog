#include "player.h"

Player *playerIni(Coord pos, Orientation ori, Life life){
	Player *play;
	play=(Player *)malloc(sizeof(Player));
	play->backpack=gdsl_list_alloc("backpack", NULL, free);
	play->pos=pos;
	play->ori=ori;
	play->life=life;
	play->weapon=NULL;
	play->hand=NULL;
	return play;
}

Coord getPlayerPos(Player *player){
	return player->pos;
}

Orientation getPlayerOri(Player *player){
	return player->ori;
}

gdsl_list_t getPlayerBackpack(Player *player){
	return player->backpack;
}

Life getPlayerLife(Player *player){
	return player->life;
}

Object *getPlayerWeapon(Player *player){
	return player->weapon;
}

void setPlayerPos(Coord pos, Player *player){
	player->pos=pos;
}

void setPlayerOri(Orientation ori, Player *player){
	player->ori=ori;
}


void setPlayerLife(Life life, Player *player){
	player->life=life;
}

void updatePlayerLife(Player *player, int incr){
	player->life+=incr;
}

void setPlayerWeapon(Player *player,Object *weapon){
	player->weapon=weapon;
}

void setPlayerHand(Player *player, Object *object){
	player->hand=object;
}

Object *getPlayerHand(Player *player){
	return player->hand;
}

void freePlayer(Player *player){
	free(player);
}



int getInventaryObjectAmount(inventaryObject *iObj){
	return iObj->amount;
}

Object *getObjectInventaryObject(inventaryObject *iObj){
	return iObj->obj;
}

void setInventaryObjectAmount(inventaryObject *iObj, int amount){
	iObj->amount=amount;
}

void setObjectInventaryObjectAmount(inventaryObject *iObj, Object *obj){
	iObj->obj=obj;
}
