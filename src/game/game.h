#ifndef gameheader
#define gameheader

#include "../general.h"
#include "world.h"
#include "stage.h"
#include "player.h"
#include "enemies.h"
#include <sys/time.h>

#define ZOMBIE_WAVE_UNIT 20

typedef struct{
	gdsl_list_t iaRooms;
}IA;

typedef struct{
	Room *currentRoom;
	Stage *currentStage;
	Player *player;	
	gdsl_list_t enemieList;
	gdsl_list_t bulletList;
	Square visibleSquare;
	IA *ia;
	Runtime runtime;
	struct timeval prevTime, timePaused;
	bool paused;
	int waveNumber;
}Game;

typedef struct{
	Coord pos;
	DIRECTION dir;
}Bullet;

typedef enum { GAME_RUNNING, GAME_PAUSED, GAME_FINISHED }GAME_STATE;

/*Primitivas de Game*/

Game *iniGame(Room *currentRoom, Stage *currentStage, World *world);

void pauseGame(Game *game);

void resumeGame(Game *game);

bool isGamePaused(Game *game);

GAME_STATE updateGameState(Game *game);

Runtime getGameRuntime(Game *game);

Room *getGameCurrentRoom(Game *game);

int getGameWaveNumber(Game *game);

int incrementWaveNumber(Game *game);

Stage *getGameCurrentStage(Game *game);

Player *getGamePlayer(Game *game);

gdsl_list_t getGameEnemies(Game *game);

gdsl_list_t getGameBullets(Game *game);

IA *getGameIA(Game *game);

Square getGameVisibleSquare(Game *game);

void setGameCurrentRoom(Room *room,Game *game);

void setGameCurrentStage(Stage *stage,Game *game);

Room *otherRoom(Game *game,Coord curr, Coord next);

void freeGame(Game *game);

/*Primitivas de Bullet*/

Bullet *createBullet(DIRECTION dir, Coord pos);

Coord getBulletPos(Bullet *bullet);

DIRECTION getBulletDir(Bullet *bullet); 

void setBulletDir(Bullet *bullet,DIRECTION dir);

void setBulletPos(Bullet *bullet, Coord pos);

long int isBulletInCoord(gdsl_element_t bullets, void *coord);

#endif
