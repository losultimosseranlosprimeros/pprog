#include "preprocesser.h"

char * _stripCaps(char * string);
char * _stripSymbols(char * string);
char * _stripPrepositions(char * string);
char ** _splitOneString(char * string);

char ** processString(char *string){

    char * result;
    char **final;

    if(!string){
        return NULL;
    }
    result = _stripCaps(string);
    result = _stripSymbols(result);
    result = _stripPrepositions(result);
    final = _splitOneString(result);
    free(result);
    return final;
}

char * _stripCaps(char * string){
    char * letra;

    for(letra = string; *letra; letra++){
        if (*letra >= 'A' &&  *letra <= 'Z'){
            *letra += 'a'-'A';
        }
    }

    return string;
}

char * _stripSymbols(char * string){

    char * letra;

    for(letra = string; *letra; letra++){
        if (!( (*letra >= 'a' &&  *letra <= 'z') || (*letra >= '0' && *letra <= '9') )){
            *letra = ' ';
        }
    }

    return string;

}

char * _stripPrepositions(char * string){

    FILE *f;
    char prep[20];
    char aux2[200];
    char *palabra;
    int i=0,j=0;
    char *aux1=strdump(string);
    f = fopen("resources/listaPrep.txt", "r");
    if (!f) return NULL;

    while(!feof(f)){
        fscanf(f,"%s",prep);
        palabra=strtok(aux1," ");
        aux2[0]='\0';
        while(palabra!=NULL){
            if(strcmp(palabra,prep)) {
                i=strlen(aux2);
                strcpy(aux2+i,palabra);
                j=strlen(aux2);
                aux2[j]=' ';
                aux2[j+1]='\0';
            }
            palabra=strtok(NULL," ");
            if(palabra==NULL){
                aux2[j]='\0'; 
            }         
       	}
       	strcpy(aux1,aux2);
    }
    fclose(f);
    return aux1;
}

char ** _splitOneString(char * string){

    char * temp;
    char **result = malloc(2 * sizeof(result[0]) + (strlen(string)+1) * sizeof(char));
    temp = (char *) (result + 2);
    strcpy(temp,string);

    result[0] = strtok(temp, " ");

    if (!result[0]){
        result[0]=temp;
        result[1]=NULL;
        return result;
    }

    result[1] = temp + 1 + strlen(result[0]);

    return result;
}

char * processWildcard(char *string, char *word){
    int i=0,num=0;
    long tamanio=0;
    char * aux = NULL;
    for(i=0;i<strlen(string);i++){
        if(string[i]=='*')
            num++;
    }
    tamanio = (strlen(string) + (num * strlen(word)) );
    aux=(char *)malloc( tamanio * sizeof(char));
    int flag=0;
    for(i=0; i < strlen(string); i++){
        if(string[i]=='*'){            
            strcpy(aux + i,word);
            aux[i + strlen(word)]=' ';
            i=i+strlen(word);
            flag=1;
        }else aux[i]=string[i];
    }
    if(flag)
        aux[tamanio-1]='\0';
    else
        aux[tamanio]='\0';

    return aux;
}
