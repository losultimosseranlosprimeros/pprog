#ifndef _commandlistheader
#define _commandlistheader

#include "../general.h"

typedef struct{

	char * key;
	cmd_func pfunc; 

}CmdListRow;

typedef struct
{
	int nRows;
	CmdListRow * list;
	
}CmdList;

CmdList * cmd_init();
void cmd_associate(CmdList * list, char * key, cmd_func pfunc);
int cmd_exec(CmdList * list, char * key, char * success, char * failure, void * parameter1, void * parameter2);
void freeCmdList(CmdList *list);

#endif