
#ifndef _interpreterheader
#define interpreterheader

#include "../controllers/generalcontroller.h"

CmdList *create_cmd_list();

void cmd_interpret(generalController * gc,char ** command);

#endif