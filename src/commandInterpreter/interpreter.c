#include "interpreter.h"
#include "commandlist.h"
#include "preprocesser.h"

#include "../controllers/generalcontroller.h"

#define checkFeof(f) if(feof(f)) return;

CmdList *create_cmd_list(){

	CmdList * list = cmd_init();
	cmd_associate(list, "cmd_help", cmd_help);
	cmd_associate(list, "cmd_MOAB", cmd_MOAB);
	cmd_associate(list, "cmd_recoger", cmd_recoger);
	cmd_associate(list, "cmd_dejar", cmd_dejar);
	cmd_associate(list, "cmd_reload", cmd_reload);
	cmd_associate(list, "cmd_heal", cmd_potion);

	return list;

}

void cmd_interpret(generalController * gc,char ** command){

	char buff[200];
	char *patterName, *success, *failure, *commandName;
	FILE * f = fopen("resources/commandlist.txt", "r");
	if (!f){
		return;
	}
	fgets(buff,200,f);
	while(!feof(f)){
		buff[strlen(buff)-1]='\0';
		logToFile(buff);
		for(patterName=buff;*patterName!='\t' && *patterName != '\0'; patterName++);
		patterName++;
		logToFile(patterName);
		if (!strcmp(patterName,command[0])){
			logToFile("escogido:");
			logToFile(patterName);
			fgets(buff,200,f);
			checkFeof(f);
			buff[strlen(buff)-1]='\0';
			success = processWildcard(buff, command[1]);

			fgets(buff,200,f);
			checkFeof(f);
			buff[strlen(buff)-1]='\0';
			failure = processWildcard(buff, command[1]);

			fgets(buff,200,f);
			checkFeof(f);
			buff[strlen(buff)-1]='\0';
			commandName = buff;

			cmd_exec(gc->commandList, commandName, success, failure, gc, command[1]);
			free(command);
			free(success);
			free(failure);
			return;

		}else{

			fgets(buff,200,f);
			checkFeof(f);
			fgets(buff,200,f);
			checkFeof(f);
			fgets(buff,200,f);
			checkFeof(f);

		}

		fgets(buff, 200, f);

	}

	cmd_default(gc);
	free(command);

	return;

}