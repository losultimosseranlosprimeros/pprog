
#include "commandlist.h"

CmdList * cmd_init(){

	CmdList * list = malloc(sizeof(list[0]));
	list->nRows = 0;
	list->list = NULL; 
	return list;
}

void cmd_associate(CmdList * lista, char * key, cmd_func pfunc){

	if (!lista || !key || !pfunc)	return;

	lista->list = realloc(lista->list, (lista->nRows+1)*sizeof(lista->list[0])) ;
	
	lista->list[lista->nRows].key = key;
	lista->list[lista->nRows].pfunc = pfunc; 
	lista->nRows++;
}

int cmd_exec(CmdList *lista, char *key, char *success, char *failure, void *parameter1, void *parameter2){

	int i;

	if (!lista || !key ) return ERR;

	for (i = 0; i < lista->nRows; ++i)
	{
		if ( !strcmp(key, lista->list[i].key) ){
			
			return (*lista->list[i].pfunc)(parameter1,parameter2,success,failure);
		}
	}

	return ERR;

}

void freeCmdList(CmdList *list){

	free(list->list);
	free(list);
	

}