#include "statuswindow.h"

#define StwFromMw statusWindow *stw = mw->data
#define StwNeedsRefresh stw->needsRefresh = YES

#define LIFE_SIZE 12
#define RUNTIME_SIZE 5
#define SPACE_SIZE 9
#define AMMO_SIZE 4
#define MESSAGE_SIZE(hsize) (hsize - SPACE_SIZE - LIFE_SIZE - RUNTIME_SIZE - AMMO_SIZE)

void setStatusWindowInitialInfo (modalWindow *mw, Runtime runtime, Life life, int ammo) {

    mw->data = realloc (mw->data, sizeof (statusWindow) );
    statusWindow *stw = mw->data;
    stw->life = life;
    stw->runtime = runtime;
    stw->needsRefresh = YES;
    stw->message = NULL;
    stw->ammo = ammo;
    stw->hsize = mw->sc->nc;

}

void updateStatusWindowAmmo (modalWindow *mw, int ammo){
    StwFromMw;
    stw->ammo = ammo;
    StwNeedsRefresh;
}

void updateStatusWindowRuntime (modalWindow *mw, Runtime runtime) {
    StwFromMw;
    stw->runtime = runtime;
    StwNeedsRefresh;
}


void updateStatusWindowLife (modalWindow *mw, Life life) {
    StwFromMw;
    stw->life = life;
    StwNeedsRefresh;
}


void displayStatusWindowMessage (modalWindow *mw, char *message) {
    StwFromMw;
    free (stw->message);
    stw->message = strdump (message);
    gettimeofday (&stw->messageInitialDisplayTime, NULL);
    StwNeedsRefresh;
}


void rewriteStatusWindow (modalWindow *mw) {

	if (!mw || !mw->data){
		return;
	}

    StwFromMw;
    if (stw->message) {
        createTimeOfDay (tv);
        int offset = timeValOffset (stw->messageInitialDisplayTime, tv);
        if (offset > MESSAGE_WINDOW_MAX_DISPLAY) {
            free (stw->message);
            stw->message = NULL;
            StwNeedsRefresh;
            char *message = calloc (MESSAGE_SIZE (stw->hsize) + 1, sizeof (char) );
            memset (message, ' ', MESSAGE_SIZE (stw->hsize) );
            chtype *chmessage = chstr(message);
            win_write_line_at(mw->sc, 0, LIFE_SIZE+AMMO_SIZE+((2*SPACE_SIZE)/3), chmessage);        
        }

    }

    if (stw->needsRefresh == NO) {
        return;
    }

    /** WRITING STATUS WINDOW **/

    /**
     * Status window structure:
     * |==-       |   ammo   message space   HH:MM
     * size: 12     3   4  3    variable   3   5
     */

    char life[LIFE_SIZE + 1], hour[RUNTIME_SIZE + 1], ammo[AMMO_SIZE+1], *message = calloc (MESSAGE_SIZE (stw->hsize) + 1, sizeof (char) );

    /* Print life */
    life[0] = life[LIFE_SIZE - 1] = '|';
    life[LIFE_SIZE] = '\0';
    int filled = (stw->life * 10) / MAX_LIFE;
    filled = filled < 0 ? 0 : filled;
    memset (life + 1, '=', filled * sizeof (char) );
    memset (life + 1 + filled, ' ', LIFE_SIZE - 2 - filled * sizeof (char) );
    if ((stw->life * 10) % MAX_LIFE){
        *(life + 1 + filled) = '-';
    }

    /* print hour */
    sprintf (hour, "%.2d:%.2d", stw->runtime.minutes, stw->runtime.seconds);

    /* print ammo */
    sprintf (ammo, "%.4d",stw->ammo);

    chtype *chlife, *chhour, *chammo, *chmessage=NULL;

    /* print message */
    if(stw->message){
        int offset = (MESSAGE_SIZE (stw->hsize) - strlen (stw->message))/2;
        offset = offset < 0 ? 0 : offset;
        memset (message, ' ', MESSAGE_SIZE (stw->hsize) );
        int written = snprintf (message + offset, MESSAGE_SIZE (stw->hsize) + 1 , "%s", stw->message);
        *(message + offset + written) = ' ';
        chmessage = chstr(message);

        win_write_line_at(mw->sc, 0, LIFE_SIZE+AMMO_SIZE+((2*SPACE_SIZE)/3), chmessage);
    }

    /* Conversion into chtypes for printing */
    chlife = chstr(life);
    chhour = chstr(hour);
    chammo = chstr(ammo);

    /* print everything into the scwindow */
    win_write_line_at(mw->sc, 0, 0, chlife);
    win_write_line_at(mw->sc, 0, LIFE_SIZE + (SPACE_SIZE/3), chammo);
    win_write_line_at(mw->sc, 0, stw->hsize - RUNTIME_SIZE, chhour);


    free(chlife);
    free(chmessage);
    free(chammo);
    free(chhour);

    stw->needsRefresh = NO;

}
