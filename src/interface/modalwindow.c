#include "modalwindow.h"
#include "menuwindow.h"
#include "mapwindow.h"
#include "commandwindow.h"
#include "textwindow.h"
#include "statuswindow.h"

void rewriteModalWindow(modalWindow * mw);

modalWindow * iniModalWindow( int height, int width, int originx, int originy, MODAL_TYPE type, int index ){

    modalWindow *ventana = (modalWindow *)malloc(sizeof(modalWindow));

    ventana->sc = win_new(originy, originx, height, width, index);
    ventana->type = type;
    ventana->data = NULL;
    ventana->subWindow = NULL;
    switch(type){
        case MODAL_MAP:
            win_resize(ventana->sc, originy+1, originx, height-1, width);
            ventana->subWindow = iniModalWindow(1, width, originx, originy, MODAL_STATUS, index);
            break;
        case MODAL_TEXT:
            win_bgcol(ventana->sc, COLOR_GREEN, NO);
            win_fgcol(ventana->sc, COLOR_BLACK, NO);
            break;
        case MODAL_COMMAND:
            win_bgcol(ventana->sc, COLOR_GREEN, NO);
            win_fgcol(ventana->sc, COLOR_BLACK, NO);
        case MODAL_OBJECTSMENU:
        case MODAL_DEFAULTMENU:
            win_bgcol(ventana->sc, COLOR_CYAN, NO);
            win_fgcol(ventana->sc, COLOR_BLACK, NO);
        default:
            break;
    }
    return ventana;

}

void animateModalWindowToSize(modalWindow * mw, DIRECTION direction, int x, int y, int w, int h){
    int i;/*comenzamos fuera de superwindow y por cada movimiento llamamos a rewriteModalWindow*/
    
    switch(direction){
        case RIGHT:
            win_resize(mw->sc, y, x, h, 0);
            for(i=1;i<=w;i++){
                rewriteModalWindow(mw);
                usleep(animationLag);
                win_resize(mw->sc, y, x, h, i);
            }
            rewriteModalWindow(mw);
            break;
            
        case LEFT:

            win_resize(mw->sc, y, x+w, LINES, 0);
            for(i=w-1;i>=0;i--){
                rewriteModalWindow(mw);
                usleep(animationLag);
                win_resize(mw->sc, y, x+i, LINES, w-i);
            }
            rewriteModalWindow(mw);
            break;
            
        case UP:
            win_resize(mw->sc, y+h, x, 0, COLS);
            for(i=h-1;i>=0;i--){
                rewriteModalWindow(mw);
                usleep(animationLag);
                win_resize(mw->sc, y+i, x, h-i, COLS);
            }
            rewriteModalWindow(mw);
            break;
            
        case DOWN:
            win_resize(mw->sc, y, x, 0, w);
            for(i=1;i<=h;i++){
                rewriteModalWindow(mw);
                usleep(animationLag);
                win_resize(mw->sc, y, x, i, w);
            }
            rewriteModalWindow(mw);
            break;
    }


}


void freeModalWindow(modalWindow * mw){
    win_delete(mw->sc);
    if (mw->type == MODAL_DEFAULTMENU || mw->type == MODAL_OBJECTSMENU){
        freeMenuWindow(mw);
    }
    if (mw->subWindow){
        free(mw->subWindow);
    }
    free(mw->data);
    free(mw);
}


void refreshModalWindow( modalWindow * mw ){
    win_refresh(mw->sc);
    switch(mw->type){
        case MODAL_OBJECTSMENU:
        case MODAL_DEFAULTMENU:
            refreshMenuWindow(mw);
            break;
        case MODAL_STATUS:
            rewriteStatusWindow(mw);
            break;
        default:
            break;
    }
    if (mw->subWindow){
        refreshModalWindow(mw->subWindow);
    }

}

void rewriteModalWindow(modalWindow * mw){
    win_cls(mw->sc);
	switch(mw->type){
		case MODAL_MAP:
			rewriteMapWindow(mw);
			break;
        case MODAL_DEFAULTMENU:
		case MODAL_OBJECTSMENU:
			menuWindowRewrite(mw);
			break;
		case MODAL_COMMAND:
			commandWindowRewrite(mw);
			break;
		case MODAL_TEXT:
			rewriteTextWindow(mw);
			break;
        case MODAL_STATUS:
            rewriteStatusWindow(mw);
            break;

	}
    refresh();
}



