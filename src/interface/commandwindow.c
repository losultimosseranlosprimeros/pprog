#include "commandwindow.h"

void commandWindowAddChar(modalWindow* mw, char character){
   int originalsize;
   chtype * string = NULL;
   if(!mw->data){
       originalsize=0;
   }
   else{
       originalsize=strlen((char*)mw->data);
   }
   mw->data=realloc(mw->data, (originalsize+2)*(sizeof(char)));
   ((char *)mw->data)[originalsize]=character;
   ((char *)mw->data)[originalsize+1]='\0';
   string = chstr(mw->data);
   win_write_line_at(mw->sc, 1, 1, string);
   free(string);
}

void commandWindowRemoveChar(modalWindow* mw){
   int originalsize;
   char space[2];
   chtype * string = NULL;
   
   if(!mw->data){
       originalsize=0;
   }else{
       originalsize=strlen((char*)mw->data);
   }
   if(!originalsize) return;
 
   mw->data=realloc(mw->data, originalsize*(sizeof(char)));
   ((char *)mw->data)[originalsize-1]='\0';
   space[0]=' ';
   space[1]=0;

   string = chstr(space);
   win_write_line_at(mw->sc, 1, originalsize, string);
   free(string);
}

char* commandWindowcommand(modalWindow* mw){
   return (char*)mw->data;
}

void commandWindowRewrite(modalWindow *mw){

  if(mw->data){
    chtype * string = chstr(mw->data);
    win_write_line_at(mw->sc, 1, 1, string);
    free(string);
  }
}
