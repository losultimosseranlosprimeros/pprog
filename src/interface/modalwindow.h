#ifndef modalwindowheader
#define modalwindowheader

#include "../general.h"
#include "scwindow.h"

#define animationLag 10000


typedef struct modalWindow_s{
	MODAL_TYPE type;
	sc_window *sc;
	void * data;
	struct modalWindow_s *subWindow;

} modalWindow; 


modalWindow * iniModalWindow( int height, int width, int originx, int originy, MODAL_TYPE type, int index );
/* Crea el scwindow inicial, guarda el tipo. 


data a NULL;
*/ 




void animateModalWindowToSize(modalWindow * mw, DIRECTION direction, int x, int y, int w, int h);
/*
Modifica su scwindow ciclo a ciclo haciendo crecer su tamaño y disminuyendo su origen.
A cada ciclo llama a la funcion refreshModalWIndow.

*/

void freeModalWindow(modalWindow * mv);
/*

Por ahora no sera animada.
libera la estructura del modal window y el scwindow.

*/

void refreshModalWindow( modalWindow * mw );
/*

refreshModalWindow:

Redibuja esta ventana.
En el caso de que la ventana sea de tipo menu, tiene que llamar a una función de esta que colorea la opcion que esté elegida.


** No puede modificar o crear scwindow **

*/

#endif
