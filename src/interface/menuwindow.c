#include "menuwindow.h"

void refreshMenuWindow(modalWindow * mw){
   menuWindowRewrite(mw);
   win_bgcol(mw->sc, mw->sc->fg_color, YES);
   win_fgcol(mw->sc, mw->sc->bg_color, YES);
   win_write_line_at(mw->sc, ((menuWindow *)mw->data)->selected*2+1, 1, ((menuWindow *)mw->data)->options[((menuWindow *)mw->data)->selected]);
   win_bgcol(mw->sc, -1, YES);
   win_fgcol(mw->sc, -1, YES);
}


void setMenuWindowOptions(modalWindow * mw, int length, chtype ** options){
   int i;
   menuWindow * menu;
   menu=(menuWindow*)malloc(sizeof(menuWindow));
   menu->selected=0;
   menu->length=length;
   menu->options=malloc(length*sizeof(menu->options[0]));
   for(i=0; i<length; i++){
      long size = (chstrlen(options[i])+1)*sizeof(options[0][0]);
      menu->options[i]=malloc(size);
      memcpy(menu->options[i],options[i],size);
   }
   mw->data=(void*)menu;
   refreshMenuWindow(mw);

}


void menuWindowChangeOption(modalWindow * mw, int diff){
   int *selected = &((menuWindow *)mw->data)->selected;
   *selected += diff;
   if( ((menuWindow *)mw->data)->length > *selected  && *selected >=0){
      refreshMenuWindow(mw);
   }else{
      *selected=(*selected +((menuWindow *)mw->data)->length)%((menuWindow *)mw->data)->length;
   }
}


int menuWindowGetOption(modalWindow * mw){
   return ((menuWindow *)mw->data)->selected;
}

void menuWindowRewrite(modalWindow * mw){
   int i;
   menuWindow * menu = (menuWindow *)mw->data;
   for (i=0; i<menu->length; i++){
      win_write_line_at(mw->sc, 2*i+1, 1, menu->options[i]);
   }

}


void freeMenuWindow(modalWindow *mw){
   int i=0;

   chtype ** options = ((menuWindow *)mw->data)->options;

   for(i=0; i<((menuWindow *)mw->data)->length; i++){
      free(options[i]);
   }
   free(options);

}
