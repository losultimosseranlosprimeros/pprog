#include "modalwindow.h"

/* en el puntero a void de modal window se va guardando lo que tiene escrito el terminal.
   Para ir añadiendo caracteres se hace un realloc de esta variable. Incluso en el caso inicial
   ya que el realloc funciona como un malloc si el puntero inicial es null. 

   escribe el caracter en la ventana y la refresca. (Para escribirlo tienes que contar cuantos caracteres
   hay escritos ya para no sobreescribir nada).

*/


void commandWindowAddChar(modalWindow* mw, char character);

void commandWindowRemoveChar(modalWindow* mw);

char *commandWindowcommand(modalWindow* mw);

void commandWindowRewrite(modalWindow *mw);