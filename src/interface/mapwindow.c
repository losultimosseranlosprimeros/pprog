
#include <stdio.h>
#include "modalwindow.h"
#include "scwindow.h"
#include "mapwindow.h"
#include <assert.h>

chtype **_createMapWindowMatrix (int height, int width);



void mapWindowMatrix (modalWindow *mw, char **matrix, int height, int width) {
    if (!matrix){
        return;
    }
    int i, j;
    chtype **newMatrix = mw->data ? mw->data : _createMapWindowMatrix (height, width);
    if (!newMatrix) {
        return;
    }
    for (i = 1; i < height; i++) {
        for (j = 0; j < width; j++) {
            newMatrix[i][j] = matrix[i][j];
        }
        win_write_line_at (mw->sc, i-1, 0, newMatrix[i]);
    }

    mw->data = newMatrix;
}

void rewriteMapWindow (modalWindow *mw) {
    int i;
    chtype **matrix = mw->data;

    for (i = 1; matrix[i] ; ++i) {
        win_write_line_at (mw->sc, i, 0, matrix[i]);
    }

}

chtype **_createMapWindowMatrix (int height, int width) {
    int i;
    if (height == 0 || width == 0) {
        return NULL;
    }
    chtype **newMatrix = calloc (1, (height + 1) * sizeof (chtype *) + (height + 1) * (width + 1) * sizeof (chtype) );
    newMatrix[0] = (chtype *) (newMatrix + height + 1);

    for (i = 0; i < height; i++) {
        newMatrix[i] = newMatrix[0] + (width + 1) * i;
    }
    newMatrix[height] = NULL;
    return newMatrix;
}