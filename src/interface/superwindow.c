#include "superwindow.h"
#include "mapwindow.h"
#include "textwindow.h"
#include "commandwindow.h"
#include "menuwindow.h"
#include "../controllers/generalcontroller.h"
#include "../controllers/controllerinclude.h"


#define currentModalWindow (sw->windows[sw->nWindows-1])
#define currentState (sw->states[sw->nWindows-1])
#define currentController (sw->controllers[sw->nWindows-1])

void _ncursesInitialization();
void _superWindowPopViewStack(superWindow * sw);
modalWindow * _superWindowNewModalWithType(superWindow * sw, SUPER_STATE state);
void _processChar(superWindow * sw, int character);


void clean_exit_on_sig(int sig_num)
{
	endwin();
    exit(sig_num);
}


void _ncursesInitialization(){

	initscr();
	noecho();
	timeout(0);
	cbreak();
	start_color();
	keypad(stdscr, TRUE);
	nodelay(stdscr, TRUE);
	curs_set(0);
	if (getenv ("ESCDELAY") == NULL){
		ESCDELAY = 25;
	}
	signal(SIGSEGV, clean_exit_on_sig);

}

superWindow * initSuperWindow(){

	superWindow * sw;

	_ncursesInitialization();

	sw = (superWindow *)malloc(sizeof(sw[0]));
	if (!sw) return NULL;

	sw->nWindows = 0;
	sw->windows = NULL;
	sw->states = NULL;
	sw->controllers = NULL;

	return sw;

}


void _superWindowPopViewStack(superWindow * sw){

	freeModalWindow(currentModalWindow);
	freeWindowController(currentController);

	sw->nWindows--;
	sw->windows = realloc(sw->windows, sw->nWindows * sizeof(sw->windows[0]));
	sw->states = realloc(sw->states, sw->nWindows * sizeof(sw->states[0]));
	sw->controllers = realloc(sw->controllers, sw->nWindows * sizeof(sw->controllers[0]));
	superWindowRefresh(sw);
}

modalWindow * _superWindowNewModalWithType(superWindow * sw, SUPER_STATE state){
	int w,h,x,y;
	MODAL_TYPE t;
	sw->nWindows++;
	sw->windows = realloc(sw->windows, sw->nWindows * sizeof(sw->windows[0]));
	sw->states = realloc(sw->states, sw->nWindows * sizeof(sw->states[0]));
	switch(state){
		case SUPER_DEFAULTMENU:
			w = 0;
			h = 0;
			x = COLS - w ;
			y = 0;
			t = MODAL_DEFAULTMENU;
			break;

		case SUPER_OBJECTSMENU:
			w = 0;
			h = 0;
			x = COLS - w ;
			y = 0;
			t = MODAL_OBJECTSMENU;
			break;

		case SUPER_TEXT:
			w = 0;
			h = 0;
			x = 0;
			y = LINES - h;
			t = MODAL_TEXT;
			break;

		case SUPER_COMMAND:
			w = 0;
			h = 0;
			x = 0;
			y = LINES -h;
			t = MODAL_COMMAND;
			break;

		case SUPER_MAP:
			w = COLS;
			h = LINES;
			x = 0;
			y = 0;
			t = MODAL_MAP;
			break;

		default:
			break;

	}
	sw->windows[sw->nWindows-1] = iniModalWindow(h, w, x, y, t, sw->nWindows);
	sw->states[sw->nWindows-1] = state;

	return currentModalWindow;

}

void _processChar(superWindow * sw, int character){


	switch(currentState){
		case SUPER_MAP:

			mapControllerProcessChar(currentController, character);
			break;

		case SUPER_COMMAND:

			commandControllerProcessChar(currentController, character);
			break;

		case SUPER_OBJECTSMENU:
		case SUPER_DEFAULTMENU:			
			menuControllerProcessChar(currentController, character);
			break;

		case SUPER_TEXT:

			textControllerProcessChar(currentController, character);
			break;

		default:
			break;
	}
	refresh();


}


void superWindowMainLoop(superWindow * sw){

	bool hasToExit = false;
	int readChar;

	while(!hasToExit){

		readChar = getch();

		if (readChar != ERR) {

			if (readChar == 27 && currentState == SUPER_MAP) {
				hasToExit = true;
			}else{
				_processChar(sw, readChar);
				superWindowRefresh(sw);
			}

		}

		if (sw->nWindows && generalControllerMainLoop(sw->controllers[0]->gc) && sw->states[0] == SUPER_MAP){
	        mapControllerRender(sw->controllers[0]);
	        superWindowRefresh(sw);
		}else if(!sw->nWindows){
			hasToExit = true;
		}
	}

	endwin();

}


void superWindowRefresh(superWindow * sw){
	int i;
	for (i =0 ; i <sw->nWindows; ++i){
		refreshModalWindow(sw->windows[i]);
	}
	refresh();

}

windowController *superWindowOpenModalWithType(superWindow * sw, MODAL_TYPE type, generalController *gc){

	windowController *wc;
	modalWindow *mw = _superWindowNewModalWithType(sw, (SUPER_STATE)type);

	wc = windowControllerInit(mw, gc);

	sw->controllers = realloc(sw->controllers, sw->nWindows * sizeof(sw->controllers[0]));
	sw->controllers[sw->nWindows-1] = wc;
	
	return wc;

}

SUPER_STATE superWindowPopWindow(superWindow * sw){
	_superWindowPopViewStack(sw);
	if (!sw->nWindows) return SUPER_NONE;
	return currentState;
}

void freeSuperWindow(superWindow *sw){

	while(sw->nWindows){
		_superWindowPopViewStack(sw);
	}

	free(sw);

}
