#ifndef _statuswindowheader
#define _statuswindowheader

#include "modalwindow.h"
#include <sys/time.h>

typedef struct {

    Runtime runtime;
    Life life;
    char *message;
    struct timeval messageInitialDisplayTime;
    bool needsRefresh;
    int hsize, ammo;

} statusWindow;

#define MESSAGE_WINDOW_MAX_DISPLAY 3000000

/**
 * Configures a new ModalWindow as a statusWindow.
 *
 * @param mw      modalwindow to configure as statusWindow
 * @param runtime Initial runtime to display
 * @param life    Initial life to display
 * @param hsize   Horizontal size of the status window
 */
void setStatusWindowInitialInfo (modalWindow *mw, Runtime runtime, Life life, int ammo);

void updateStatusWindowAmmo (modalWindow *mw, int ammo);
/**
 * Updates statusWindow Runtime
 *
 * @param mw      modalwindow to update
 * @param runtime new runtime
 */
void updateStatusWindowRuntime (modalWindow *mw, Runtime runtime);

/**
 * Updates statusWindow Life
 *
 * @param mw   modalWindow to update
 * @param life new life.
 */
void updateStatusWindowLife (modalWindow *mw, Life life);

/**
 * Displays a new message in the statusWindow
 *
 * @param mw      modalWindow in which to show it
 * @param message Message to display. It is copied so remember to FREE it.
 */
void displayStatusWindowMessage (modalWindow *mw, char *message);

/**
 * Redisplays entire statusWindow
 *
 * @param mw modalWindow containing statusWindow.
 */
void rewriteStatusWindow (modalWindow *mw);

#endif
