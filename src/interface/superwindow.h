#ifndef superwindowheader

#define superwindowheader

#include "signal.h"
#include "modalwindow.h"
#include "../controllers/generalcontroller.h"
#include "../general.h"

typedef enum {SUPER_MAP=MODAL_MAP,SUPER_DEFAULTMENU=MODAL_DEFAULTMENU,SUPER_OBJECTSMENU=MODAL_OBJECTSMENU,SUPER_COMMAND=MODAL_COMMAND,SUPER_TEXT=MODAL_TEXT, SUPER_NONE}SUPER_STATE;

typedef struct{

	modalWindow ** windows;
	int nWindows;
	SUPER_STATE * states;
  windowController ** controllers;

}superWindow;

superWindow * initSuperWindow();
/* Crea la estructura y le asigna los valores por defecto.
       El estado por defecto es SUPER_MAP
       El numero de ventanas es 1
       Hacemos alloc del array de punteros a modalWindows creando un mapWindow en la primera posicion.

       Estaria bien enviar ya de paso un mapa de prueba al mapWindow


    Luego se inicia el loop principal.
*/



void superWindowMainLoop(superWindow * sw);
/*

   Coge un caracter del terminal y llama a la funcion interpretar caracter.
   El bucle solo termina cuando se pulsa la tecla esc (27).

*/


/*void processChar(superWindow * sw, char character);


   Dependiendo de el estado en el que este la ventana se hacen diferentes acciones.

	Por ahora solo vamos a poner que estando en el mapa si se pulsa m aparece un menu,
        si se pulsa t un texto, etc...

   Si esta en el menu, se sale con q y las flechas se pasan a el menu mediante la funcion menuWindowChangeOption(modalWindow * mw, int diff);

   Las flechas son las constantes ARROW_UP, ARROW_DOWN.

   En el terminal se escribe cualquier caracter que pase excepto el intro que cierra el terminal.

   En el textwindow cualquier tecla lo cierra.

*/


void superWindowRefresh(superWindow * sw);

/*
Refresca todas las ventanas del array EN ORDEN. 

*/


windowController *superWindowOpenModalWithType(superWindow * sw, MODAL_TYPE type, generalController *gc);
/* 

Crea un nuevo controlador y modal window del tipo especificado 
*/

SUPER_STATE superWindowPopWindow(superWindow * sw);

void freeSuperWindow(superWindow *sw);

#endif
