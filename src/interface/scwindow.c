
#include "scwindow.h"

chtype ** charMatrixWithSize(int, int);

sc_window *win_new(int r1,int c1,int nr, int nc, int index){	
	
	sc_window * ventana = (sc_window *)malloc(sizeof(sc_window));
	ventana->r1=r1;
	ventana->c1=c1;
	ventana->nr=nr;
	ventana->nc=nc;
	ventana->fg_color=DEFAULTFG;
	ventana->bg_color=DEFAULTBG;
	ventana->fg_color_sec=-1;
	ventana->bg_color_sec=-1;
	ventana->index = 2*index;

	ventana->matrix = charMatrixWithSize(nr,nc);

	return ventana;
}

void win_cls(sc_window * sc){

	int color_index,i,j;
	if (!sc)
		return;

	if (sc->bg_color_sec!=-1 && sc->fg_color_sec!=-1){
		color_index = sc->index + 1;
		init_pair(color_index, sc->fg_color_sec, sc->bg_color_sec);
	}else{
		color_index = sc->index;
		init_pair(color_index, sc->fg_color, sc->bg_color);
	}

	for (i=0 ; i < sc->nr ; i++){
		move((sc->r1)+i,sc->c1);
		for (j=0 ; j < sc->nc ; j++)
			addch(' '|COLOR_PAIR(color_index));
	}
}

void win_delete(sc_window *sc){
	free((void*)sc->matrix);
	free((void *)sc);
}

int win_bgcol(sc_window *sc, int col, int sec){
   if(sc && BGMIN<=col && col<=BGMAX && !sec){
       sc->bg_color=col;
       return 1;
   }else if(sec){
   		sc->bg_color_sec = col;
   		return 1;
   }
   return 0;
}

int win_fgcol(sc_window *sc, int col, int sec){
   if(sc && FGMIN<=col && col<=FGMAX && !sec){
       sc->fg_color = col;
       return 1;
   }else if(sec){
   		sc->fg_color_sec = col;
   		return 1;
   }
   return 0;
}

int win_write_line_at(sc_window *sc, int r, int c, chtype *str){

	chtype* letra=str;
	int color_index,i = c; /* columna actual */

	if (!str || !sc || r > (sc->nr))
		return 0;

	if (sc->bg_color_sec!=-1 && sc->fg_color_sec!=-1){
		color_index = sc->index+1;
		init_pair(color_index, sc->fg_color_sec, sc->bg_color_sec);
	}else{
		color_index = sc->index;
		init_pair(color_index, sc->fg_color, sc->bg_color);
	}

	move((sc->r1)+r,(sc->c1)+c);

	while ((i<(sc->nc)) && ((*letra & bitmask)!=0) && ((*letra & bitmask)!='\n')){
		addch(*letra | COLOR_PAIR(color_index));
		sc->matrix[r][i]=*letra;
		i++;
		letra++;
	}
	return (letra - str);
}

int win_write_at(sc_window *sc, int r, int c, chtype *str){

	chtype* letra=str;
	int i = c; /* columna actual */
	int j = r; /* fila actual */
	int color_index,count=0; /* caracteres impresos */

	if (!sc || !str){
		return 0;
	}

	if (sc->bg_color_sec!=-1 && sc->fg_color_sec!=-1){
		color_index = sc->index+1;
		init_pair(color_index, sc->fg_color_sec, sc->bg_color_sec);
	}else{
		color_index = sc->index;
		init_pair(color_index, sc->fg_color, sc->bg_color);
	}

	move((sc->r1)+j,(sc->c1)+c);

	while ((j<(sc->nr)) && ((*letra & bitmask) != 0)){

		if ((*letra & bitmask) =='\n'){
			j++;
			move((sc->r1)+j,(sc->c1)+c);
			i = c;
		}else if(i < (sc->nc)){
			addch(*letra|COLOR_PAIR(color_index));
			sc->matrix[j][i]=*letra;
			i++;
			count ++;
		}
		letra ++;
	}
	return count;
}

void win_refresh(sc_window *sc){

	int color_index, act_c, act_r;	

	if(!sc)
		return;

	if (sc->bg_color_sec!=-1 && sc->fg_color_sec!=-1){
		color_index = sc->index+1;
		init_pair(color_index, sc->fg_color_sec, sc->bg_color_sec);
	}else{
		color_index = sc->index;
		init_pair(color_index, sc->fg_color, sc->bg_color);
	}

	for(act_r=0;act_r < (sc->nr);act_r++){
		move((sc->r1)+act_r,sc->c1);
		for(act_c=0;act_c < (sc->nc);act_c++)
			addch(sc->matrix[act_r][act_c]|COLOR_PAIR(color_index));
	}
}

void win_resize(sc_window *sc, int r1, int c1, int nr, int nc){
	
	sc->r1=r1;
	sc->c1=c1;
	sc->nr=nr;
	sc->nc=nc;

	free(sc->matrix);
	sc->matrix=charMatrixWithSize(nr, nc);

}

chtype ** charMatrixWithSize(int height, int width){

	int i,j;
	chtype** matriz;

	if(height == 0 || !width){
		return NULL;
	}

	matriz = malloc((height)*(width)*sizeof(chtype) + (height)*sizeof(chtype*));
	matriz[0] = (chtype *)(matriz + height);
	for(i=0;i<(height);i++){
		matriz[i]=matriz[0] + (i*(width));
		for(j=0;j<(width);j++)
			*(matriz[i]+j)=' ';
	}
	return matriz;
}
