#ifndef _menuwindowheader
#define _menuwindowheader

#include "modalwindow.h"


typedef struct {
	
	int length, selected;
	chtype ** options;

}menuWindow;

void refreshMenuWindow(modalWindow * mw);
/*
Refresca su scwindow y resalta la opcion elegida.
*/



void setMenuWindowOptions(modalWindow * mw,int length, chtype ** options);
/*

COPIA el array de opciones.

*/



void menuWindowChangeOption(modalWindow * mw, int diff);
/*
diff es +1 si se va a la opcion de abajo y -1 si a la de arriba.
Comprueba si se puede avanzar.
si se puede cambia la opcion y llama a refreascar
*/


int menuWindowGetOption(modalWindow * mw);
/* devuelve la opcion seleccionada */

void menuWindowRewrite(modalWindow * mw);

void freeMenuWindow(modalWindow *mw);


#endif