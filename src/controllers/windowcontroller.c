#include "windowcontroller.h"

windowController *windowControllerInit(modalWindow *mw, generalController *gc){

	windowController *wc = malloc(sizeof(windowController));
	if (!wc) return NULL;

	wc->gc = gc;
	wc->mw = mw;
	wc->data=NULL;

	return wc;

}


void freeWindowController(windowController *wc){

	free(wc->data);
	free(wc);

}