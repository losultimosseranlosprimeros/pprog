#include "maprenderer.h"

typedef struct {
    Coord ulc, lrc;
    char **map;
} _mapDrawWallsHelper;

char **mapRendererSpaceMatrixWithSize(int cols, int rows);
int _mapRendererDrawRoom(const gdsl_element_t E, gdsl_location_t LOCATION, void *USER_DATA);
void _printCharInPosition(char character, int x, int y, char **image, Square bounds);
int _drawEnemy(gdsl_element_t element ,gdsl_location_t location, void *data);
int _drawBullet(gdsl_element_t element ,gdsl_location_t location, void *data);

char **mapRendererRenderMap(Game *game, World *world){
    int cols, rows;
    char **matrix;
    gdsl_list_t list;
    Coord ulc = getGameVisibleSquare(game).ulc,lrc = getGameVisibleSquare(game).lrc;
    cols = lrc.x - ulc.x;
    rows = lrc.y - ulc.y;
    lrc.x-=1;
    lrc.y-=1;
    Square sq = createSquare(ulc, lrc);
    list = gdsl_list_filtered_copy(getMapRooms(getStageMap(getGameCurrentStage(game))), visibleRoomFilter, &sq);
    matrix = mapRendererSpaceMatrixWithSize (cols, rows);

    mapRendererDrawRooms (matrix, list, ulc, lrc);
    mapRendererDrawVoids (matrix, createSquare(ulc, lrc), list);
    mapRendererDrawObjects(matrix, createSquare(ulc, lrc), list, getWorldObjects(world));
    mapRendererDrawBullets(matrix, createSquare(ulc, lrc), getGameBullets(game));
    mapRendererDrawEnemies(matrix, createSquare(ulc, lrc), getGameEnemies(game));
    mapRendererDrawPlayer(matrix, getGamePlayer(game),createSquare(ulc, lrc));

    gdsl_list_free(list);

    return matrix;
}

void _printChar (char ch, int x, int y, char **image) {
    if (image[y][x] != ' ' && image[y][x] != ch)
        image[y][x] = '+';
    else {
        image[y][x] = ch;
    }
}

void mapRendererDrawRooms (char **image, gdsl_list_t roomList, Coord ulc, Coord lrc) {
    _mapDrawWallsHelper helper;
    helper.map = image;
    helper.ulc = ulc;
    helper.lrc = lrc;
    gdsl_list_map_forward (roomList, _mapRendererDrawRoom, &helper);
}


int _mapRendererDrawRoom (const gdsl_element_t elem, gdsl_location_t location, void *data) {
    int i;
    Coord rlrc, rulc;
    _mapDrawWallsHelper *helper;
    Room *room = elem;
    rlrc = room->lrc;
    rulc = room->ulc;

    helper = (_mapDrawWallsHelper *)data;
    if (isPointBetweenCoords(rulc.x, rulc.y, helper->ulc, helper->lrc) ){
        helper->map[rulc.y - helper->ulc.y][rulc.x - helper->ulc.x] = '+';
    }
    if (isPointBetweenCoords(rlrc.x, rulc.y, helper->ulc, helper->lrc) ){
        helper->map[rulc.y - helper->ulc.y][rlrc.x - helper->ulc.x] = '+';
    }
    if (isPointBetweenCoords(rulc.x, rlrc.y, helper->ulc, helper->lrc) ){
        helper->map[rlrc.y - helper->ulc.y][rulc.x - helper->ulc.x] = '+';
    }
    if (isPointBetweenCoords(rlrc.x, rlrc.y, helper->ulc, helper->lrc) ){
        helper->map[rlrc.y - helper->ulc.y][rlrc.x - helper->ulc.x] = '+';
    }

    for (i = rulc.x + 1; i < rlrc.x; i++) {
        if (isPointBetweenCoords(i, rulc.y, helper->ulc, helper->lrc) )
            _printChar ('-', i - helper->ulc.x, rulc.y - helper->ulc.y, helper->map);
        if (isPointBetweenCoords(i, rlrc.y, helper->ulc, helper->lrc) )
            _printChar ('-', i - helper->ulc.x, rlrc.y - helper->ulc.y, helper->map);
    }

    for (i = rulc.y + 1; i < rlrc.y; i++) {
        if (isPointBetweenCoords(rulc.x, i, helper->ulc, helper->lrc) )
            _printChar ('|', rulc.x - helper->ulc.x, i - helper->ulc.y, helper->map);
        if (isPointBetweenCoords(rlrc.x, i, helper->ulc, helper->lrc) )
            _printChar ('|', rlrc.x - helper->ulc.x, i - helper->ulc.y, helper->map);
    }

    return GDSL_MAP_CONT;

}


void mapRendererDrawVoids (char **image, Square bounds, gdsl_list_t roomList) {
    int numrooms, numroomvoids, i, j;
    Coord posfinal;
    Room *roomaux;
    RoomVoid *voidroomlist;

    numrooms = gdsl_list_get_size (roomList);

    for (i = 1; i <= numrooms; i++) {
        roomaux = (Room *) gdsl_list_search_by_position (roomList, i);
        numroomvoids = gdsl_list_get_size (roomaux->roomVoids);
        for (j = 1; j <= numroomvoids; j++) {
            voidroomlist = gdsl_list_search_by_position (roomaux->roomVoids, j);
            posfinal = coordSum (roomaux->ulc, voidroomlist->pos);
            _printCharInPosition(' ', posfinal.x, posfinal.y, image, bounds);
        }
    }
}


void mapRendererDrawObjects (char **image, Square bounds, gdsl_list_t roomList, gdsl_list_t objects) {
    int numrooms, numroomobjects, i, j;
    Coord posfinal;
    Room *roomaux;
    RoomObject *objectroomlist;
 	Object *objectaux;

    numrooms = gdsl_list_get_size(roomList);

    for (i=1; i<=numrooms; i++) {
        roomaux = gdsl_list_search_by_position(roomList, i);
        numroomobjects = gdsl_list_get_size(roomaux->objects);
        for (j = 1; j <= numroomobjects; j++) {
            objectroomlist = gdsl_list_search_by_position(roomaux->objects, j);
            objectaux = objectroomlist->object;
            posfinal = coordSum(roomaux->ulc, objectroomlist->pos);
            _printCharInPosition(objectaux->character, posfinal.x, posfinal.y, image, bounds);
        }
    }
}

void mapRendererDrawEnemies (char **image, Square bounds, gdsl_list_t enemies){

    Image img;
    img.bounds = bounds;
    img.image = image;
    gdsl_list_map_forward(enemies, _drawEnemy, &img);

}

int _drawEnemy(gdsl_element_t element ,gdsl_location_t location, void *data){
    Enemie *enemy = element;
    Image * image = data;
    char printed;
    switch(enemy->ori){
        case UP:
            printed = 'M';
            break;
        case DOWN:
            printed = 'W';
            break;
        case LEFT:
            printed = 'E';
            break;
        default:
        case RIGHT:
            printed ='3';
            break;
    }
    _printCharInPosition(printed, enemy->pos.x, enemy->pos.y, image->image, image->bounds);
    return GDSL_MAP_CONT;
}

void mapRendererDrawPlayer (char **image, Player *player, Square bounds) {
    
    switch(player->ori){
        case UP:
            _printCharInPosition('^',player->pos.x,player->pos.y,image,bounds);
            break;
        case DOWN:
            _printCharInPosition('v',player->pos.x,player->pos.y,image,bounds);
            break;
        case LEFT:
            _printCharInPosition('<',player->pos.x,player->pos.y,image,bounds);
            break;
        case RIGHT:
            _printCharInPosition('>',player->pos.x,player->pos.y,image,bounds);
            break;
    }
}

void mapRendererDrawBullets (char **image, Square bounds, gdsl_list_t bullets){
    Image img;
    img.bounds=bounds;
    img.image=image;
    gdsl_list_map_forward(bullets,_drawBullet,&img);
}

int _drawBullet(gdsl_element_t element ,gdsl_location_t location, void *data){
    Bullet *bullet = (Bullet *)element;
    Image *img = data;
    switch(bullet->dir){
        case UP:
        case DOWN:
            _printCharInPosition('|',bullet->pos.x,bullet->pos.y,img->image,img->bounds);
            break;
        case LEFT:
        case RIGHT:
            _printCharInPosition('-',bullet->pos.x,bullet->pos.y,img->image,img->bounds);
            break;
    }
    return GDSL_MAP_CONT;
}

char **mapRendererSpaceMatrixWithSize (int cols, int rows) {
    if (cols == 0 || rows == 0){
        return NULL;
    }
    int i, j;
    char **matrix = malloc (rows * sizeof (matrix[0]) + rows * cols * sizeof (matrix[0][0]) );
    matrix[0] = (char *) (matrix + rows);
    for (i = 0; i < rows; ++i) {
        matrix[i] = matrix[0] + i * cols;
        for (j = 0; j < cols; j++) {
            matrix[i][j] = ' ';
        }
    }
    return matrix;
}

void _printCharInPosition (char character, int x, int y, char **image, Square bounds){

    if (isPointBetweenCoords(x, y, bounds.ulc, bounds.lrc)) {
        Coord pos = coordDiff(createCoord(x, y), bounds.ulc);
        image[pos.y][pos.x]=character;
    }

}



