#include "commandcontroller.h"
#include "../commandInterpreter/interpreter.h"
#include "../commandInterpreter/preprocesser.h"

/* Recibe un commandWindow (hay que hacer casting) y un generalController que se tiene que guardar. 
   Devuelve la estructura en la que ha guardado estos dos */

/*

Crea la estructura de windowController (malloc)
y devuelve la estructura.

*/ 

void commandControllerStartWithDefault(windowController *wc){
	pauseGame(wc->gc->game);
	animateModalWindowToSize(wc->mw, UP, 0, LINES-3, COLS, 3);
}


void commandControllerProcessChar(windowController *wc, int character){
	char * command;

	switch(character){

		case 27:
			generalControllerCloseWindow(wc->gc);
			break;

		case '\n':
			command = commandWindowcommand(wc->mw);
			/* interperetar comando command */
			if (command) {
				cmd_interpret(wc->gc,processString(command));
			}
			/* limpiar la barra de comandos */
			break;

		case KEY_BACKSPACE:
		case 127:
			commandWindowRemoveChar(wc->mw);
			break;

		default:
			commandWindowAddChar(wc->mw, (char)character);
			break;

	}
}
