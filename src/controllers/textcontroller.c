#include "textcontroller.h"

void textControllerStartWithString(windowController *wc, char * str){

	pauseGame(wc->gc->game);
	setTextWindowText(wc->mw, str);
	animateModalWindowToSize(wc->mw, UP, 0, LINES-10, COLS, 10);
	
}

void textControllerProcessChar(windowController *wc, int character){

	switch(character){
		case 27:
			generalControllerCloseWindow(wc->gc);
			break;
		default:
			break;
	}

}
