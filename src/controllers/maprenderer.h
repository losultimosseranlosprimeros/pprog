#include "../general.h"
#include "../game/game.h"
#include "../game/world.h"
#include "../game/player.h"

char **mapRendererRenderMap(Game *game, World *world);

gdsl_list_t mapRendererFilterRooms(Map *map, Coord ulc, Coord lrc);

void mapRendererDrawRooms(char **image, gdsl_list_t roomList, Coord ulc, Coord lrc);

void mapRendererDrawVoids(char **image, Square bounds, gdsl_list_t roomList);

void mapRendererDrawObjects (char **image, Square bounds, gdsl_list_t roomList, gdsl_list_t objects);

void mapRendererDrawPlayer (char **image, Player *player, Square bounds);

void mapRendererDrawEnemies (char **image, Square bounds, gdsl_list_t enemies);

void mapRendererDrawBullets (char **image, Square bounds, gdsl_list_t bullets);