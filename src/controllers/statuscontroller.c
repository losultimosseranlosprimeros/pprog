#include "statuscontroller.h"
#include <stdarg.h>

static windowController *__defaultStatusController__ = NULL;

windowController * statusControllerStartWithDefault(modalWindow *mw, generalController *gc){
	if (__defaultStatusController__){
		freeWindowController(__defaultStatusController__);
	}
	Runtime runtime;
	runtime.minutes = 0;
	runtime.seconds = 0;
	setStatusWindowInitialInfo(mw, runtime, MAX_LIFE, 0);
	return (__defaultStatusController__ = windowControllerInit(mw, gc));
}

void statusControllerUpdate(Game *game){

	windowController *wc = __defaultStatusController__;
	if (!wc) return;
	Object *weapon;
	if ((weapon = getPlayerWeapon(getGamePlayer(game))))
	{
		int ammo = getWeaponAmmo(getObjectWeapon(weapon));
		updateStatusWindowAmmo(wc->mw, ammo);
	}
	updateStatusWindowLife(wc->mw, getPlayerLife(getGamePlayer(game)));
	updateStatusWindowRuntime(wc->mw, getGameRuntime(game));
}

void statusControllerDisplayMessage(char *message, ...){
	windowController *wc = __defaultStatusController__;
	if (!wc) return;
	char buffer[1000];
	va_list argptr;
	va_start(argptr, message);
	vsnprintf(buffer, 1000, message, argptr);
	va_end(argptr);
	displayStatusWindowMessage(wc->mw, buffer);
}
