#ifndef _windowcontrollerheader
#define _windowcontrollerheader

#include "generalcontroller.h"

typedef struct{
	generalController * gc;
	modalWindow * mw;
	void * data;
}windowController;

windowController *windowControllerInit(modalWindow *mw, generalController *gc);
void freeWindowController(windowController *wc);


#endif