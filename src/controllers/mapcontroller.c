#include "mapcontroller.h"
#include "statuscontroller.h"
#include "maprenderer.h"
#include "../logic/includes.h"

void _mapControllerMoveCoords (windowController *wc, int x, int y);

void mapControllerStartWithDefault (windowController *wc) {

    /** Create with status window controller */

    statusControllerStartWithDefault(wc->mw->subWindow, wc->gc);

    char **matrix;
    int i;

    Coord ulc, lrc;
    ulc.x = ulc.y = 0;
    lrc.x = COLS;
    lrc.y = LINES;

    wc->data = malloc (sizeof (MapController) + (lrc.y* sizeof(char *)) + (lrc.y * lrc.x * sizeof(char)) );

    ( (MapController *) wc->data)->bounds = createSquare (ulc, lrc);
    matrix = ( (MapController *) wc->data)->image = (char **)(( (MapController *) wc->data)+1);
    matrix[0] = (char *)(matrix + lrc.y);
    for (i = 0; i < lrc.y; ++i) {
        matrix[i]=matrix[0]+i*(lrc.x);
    }

    _mapControllerMoveCoords (wc, 0, 0);
    mapControllerRender(wc);

}

void mapControllerProcessChar (windowController *wc, int character) {


    switch (character) {

        case 't':
        case 'T':
            generalControllerCreateTextWindow (wc->gc, "Hola");
            break;

        case 'c':
        case 'C':
            generalControllerCreateCommandWindow (wc->gc);
            break;

        case 'm':
        case 'M':
            generalControllerCreateDefaultMenu (wc->gc);
            break;

        case KEY_UP:
            _mapControllerMoveCoords (wc, 0, -1);
            break;

        case KEY_DOWN:
            _mapControllerMoveCoords (wc, 0, 1);
            break;

        case KEY_LEFT:
            _mapControllerMoveCoords (wc, -1, 0);
            break;

        case KEY_RIGHT:
            _mapControllerMoveCoords (wc, 1, 0);
            break;

        case ' ':
            if (ologic_pickObj(wc->gc->game) == ERR){
                ologic_dropObj(wc->gc->game);
            }
            break;

        case 'f':
        case 'F':
            ologic_keepObj(wc->gc->game, YES);
            break;

        case 'a':
        case 'A':
            plogic_shoot(wc->gc->game);
            break;

        case 'r':
        case 'R':
            /*recargar*/
            break;

        default:
            break;

    }

    mapControllerRender (wc);

}

void mapControllerRender (windowController *wc) {

    MapController *mc = wc->data;
    char **map = mc->image;
    Coord size = squareSize(mc->bounds);

    map =  mapRendererRenderMap(wc->gc->game, wc->gc->world);

    mapWindowMatrix (wc->mw, map, size.y, size.x);

    free (map);

}

void _mapControllerMoveCoords (windowController *wc, int x, int y) {

    MapController *mc = ( (MapController *) wc->data);

    mlogic_movePlayer(wc->gc->game, x, y);
    Coord size = coordDiff(mc->bounds.lrc, mc->bounds.ulc);
    Coord newUlc = mlogic_terminal_origin(wc->gc->game, size.x, size.y);
    mc->bounds = moveSquareUlc(mc->bounds, newUlc);
    wc->gc->game->visibleSquare = mc->bounds;

}
