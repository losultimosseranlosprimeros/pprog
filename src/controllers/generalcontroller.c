#include "generalcontroller.h"
#include "controllerinclude.h"
#include "../interface/superwindow.h"
#include "../commandInterpreter/interpreter.h"
#include "../logic/fightlogic/flogicmain.h"
#include "maprenderer.h"

generalController * generalControllerStart(){

	windowController * wc;
	generalController * gc = malloc(sizeof(gc[0]));
	if (!gc) return NULL;

	gc->sw = initSuperWindow();
	if (!gc->sw) return NULL;

	gc->commandList = create_cmd_list();
	if (!gc->commandList) return NULL;

	json_error_t error;
	json_t *root = json_load_file("resources/world.json", 0, &error);
	gc->world = iniWorld( root );
	json_decref(root);
	
	Stage *stage;
	stage=gdsl_list_get_head(gc->world->stages);
	gc->game=iniGame(getRoomFromCoord(stage->map.posI,&stage->map), stage,gc->world);

	srand(time(NULL));

	wc = superWindowOpenModalWithType(gc->sw, MODAL_MAP, gc);

	mapControllerStartWithDefault(wc);

	resetTimer();

	superWindowMainLoop(gc->sw);

	return gc;

}

bool generalControllerMainLoop(generalController *gc){

	if (isGamePaused(gc->game) == YES){
		return NO;
	}
	bool ret = flogic_run_cycle(gc->game,gc->world);
	statusControllerUpdate(gc->game);
	if (updateGameState(gc->game) == GAME_FINISHED){
		generalControllerCloseWindow(gc);
		generalControllerCreateTextWindow(gc, "Has perdido!\n\nPulsa ESC para salir.");
	}
	return ret;
}

void generalControllerCloseWindow(generalController *gc){
	if( superWindowPopWindow(gc->sw) == SUPER_MAP){
		resumeGame(gc->game);
	}
}

void generalControllerCreateDefaultMenu( generalController *gc ){
	windowController * wc = superWindowOpenModalWithType(gc->sw, MODAL_DEFAULTMENU, gc);
	menuControllerStartWithDefault(wc);
}
/* Crea un menu general */

void generalControllerCreateObjectsMenu( generalController *gc ){
	windowController * wc = superWindowOpenModalWithType(gc->sw, MODAL_OBJECTSMENU, gc);
	menuControllerStartWithObjects(wc);
}
/*crea un menu de objetos*/

void generalControllerCreateCommandWindow( generalController *gc ){
	windowController * wc = superWindowOpenModalWithType(gc->sw, MODAL_COMMAND, gc);
	commandControllerStartWithDefault(wc);
}
/* crea una ventana de comandos */

void generalControllerCreateTextWindow(generalController *gc, char *text){
	windowController * wc = superWindowOpenModalWithType(gc->sw, MODAL_TEXT, gc);
	textControllerStartWithString(wc, text);
}
/* Crea una ventana de texto con el texto especificado */

void freeGeneralController(generalController *gc){
	freeGame(gc->game);
	freeSuperWindow(gc->sw);
	freeCmdList(gc->commandList);
	freeWorld(gc->world);
	free(gc);

}
