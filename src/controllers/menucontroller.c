#include "menucontroller.h"

void _optionDefaultInterpreter(windowController *wc, int option);
void _optionObjectsInterpreter(windowController *wc, int option);

void menuControllerStartWithDefault(windowController *wc){
	pauseGame(wc->gc->game);
	chtype *opcionesMenu[3];
	opcionesMenu[0]=chstr("objetos");
	opcionesMenu[1]=chstr("salir");
	setMenuWindowOptions(wc->mw, 2, opcionesMenu);	
	free(opcionesMenu[0]);
	free(opcionesMenu[1]);
	animateModalWindowToSize(wc->mw, LEFT, COLS-40, 1, 40, 30);
}

void menuControllerStartWithObjects(windowController *wc){
	int i,tamanio;
	gdsl_list_t backpack=getPlayerBackpack(getGamePlayer(wc->gc->game));
	Object *obj;
	char phrase[20];
	if(!backpack)
		return;
	tamanio=gdsl_list_get_size(backpack);
	logToFile("cogemos el tamanio de la lista: %d",tamanio);

	chtype *objetos[tamanio];
	for(i=1;i<=tamanio;i++){
		obj=gdsl_list_search_by_position(backpack,i);
		sprintf(phrase, "%s x%d",obj->description,obj->amount);
		objetos[i-1]=chstr(phrase);
	}
	setMenuWindowOptions(wc->mw,tamanio,objetos);
	for(i=0;i<tamanio;i++)
		free(objetos[i]);
	//animateModalWindowToSize(wc->mw, LEFT, COLS-40, 0, 40, 30);
	win_resize(wc->mw->sc, 1, COLS-40, 30, 40);
}



void menuControllerProcessChar(windowController *wc, int character){
	int type=wc->mw->type;
	int option;
	switch(character){
		case 27:
			generalControllerCloseWindow(wc->gc);
			break;

		case '\n':
			option = menuWindowGetOption(wc->mw);
			switch(type){
				case MODAL_DEFAULTMENU:
					_optionDefaultInterpreter(wc,option);
					break;
				case MODAL_OBJECTSMENU:
					_optionObjectsInterpreter(wc,option);
					break;
			}
			break;

		case KEY_UP:
			menuWindowChangeOption(wc->mw, -1);
			break;

		case KEY_DOWN:
			menuWindowChangeOption(wc->mw, 1);
			break;

		default:
			break;
	}

}



void _optionDefaultInterpreter(windowController *wc, int option){
	gdsl_list_t backpack=getPlayerBackpack(getGamePlayer(wc->gc->game));
	char *error;
	switch(option){
		case 0:
			logToFile("creando generalController de menu de objetos");
			if(gdsl_list_get_size(backpack))
				generalControllerCreateObjectsMenu(wc->gc);
			else{
				error="No tienes objetos en el inventario.";
				generalControllerCreateTextWindow(wc->gc,error);
			}
			break;
		case 1:
			generalControllerCloseWindow(wc->gc);
			break;
		default:
			break;
	}
}

void _optionObjectsInterpreter(windowController *wc, int option){
	gdsl_list_t backpack=getPlayerBackpack(getGamePlayer(wc->gc->game));
	Object *obj=gdsl_list_search_by_position(backpack,option+1);
 	char *message=(char *)malloc(sizeof(char)*100);		
	if(isObjectAvailable(obj)){
		if(isObjectWeapon(obj)){
			plogic_changeWeapon(wc->gc->game,obj);
		}else{
			if (cmd_exec(wc->gc->commandList, obj->cmd_equip, "", "", wc->gc->game, obj) != OK){
				statusControllerDisplayMessage("No has podido usar %s",getObjectDescription(obj));
			}
			/*switch(	obj->character ){
				case 'a':
					if(plogic_reload(wc->gc->game,obj)==OK)
						gdsl_list_remove(backpack,pointerCmp,obj);
					else{
						sprintf(message,"No has podido usar %s",getObjectDescription(obj));
						statusControllerDisplayMessage(message);
						free(message);
					}
					break;
				case 'b':
					break;
				default:
					break;
			}*/
		}
		generalControllerCloseWindow(wc->gc);
	}
}