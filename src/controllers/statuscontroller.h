#include "generalcontroller.h"
#include "../interface/statuswindow.h"


windowController * statusControllerStartWithDefault(modalWindow *mw, generalController *gc);

void statusControllerUpdate(Game *game);

void statusControllerDisplayMessage(char *message, ...);
