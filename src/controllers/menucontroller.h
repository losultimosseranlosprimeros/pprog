

#include "generalcontroller.h"
#include "../interface/menuwindow.h"
#include "../logic/playerlogic.h"
#include "statuscontroller.h"

void menuControllerStartWithDefault(windowController * wc);
void menuControllerStartWithObjects(windowController * wc);


void menuControllerProcessChar(windowController *wc, int character);