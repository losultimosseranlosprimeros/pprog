/* Se encarga de controlar el modal window corresponidnete al mapa
 * y de ejecutar las acciones pertinentes cuando un evento ocurre sobre él */

#include "generalcontroller.h"
#include "../interface/mapwindow.h"

typedef struct{
	char **image;
	Square bounds;
}MapController;


void mapControllerStartWithDefault(windowController *wc);
/* llama a las funciones iniciales del mapa */


void mapControllerProcessChar(windowController *wc, int character);
/* Recibe un window controller (que es un mapcontroller) y interpreta el caracter pulsado
 * llamando a la función de `generalController correspondiente. */

/* Si el caracter es m abre un menu,
	si es c abre una ventana de comandos
	si es t abre una ventana de textos con xualquier texto 
	si es esc tiene que salir llamando a una funcín de superwindow. superwindowExitCurrentModal(superwindow *sw).

	(todo esto son llamadas a funciones de general controller con el general controller que hay en la estructura de windowcontroller)

*/

void mapControllerRender (windowController *wc);
