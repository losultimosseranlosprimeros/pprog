#include "commands.h"
#include "../logic/playerlogic.h"

int cmd_recoger(void *param1, void *param2, char *success, char *failure){

	char *result;

	if (rand()%2){
		result = success;
	}else{
		result = failure;
	}

	generalControllerCreateTextWindow((generalController *)param1, result);

	return (int)(result != success);

}

int cmd_dejar(void *param1, void *param2, char *success, char *failure){

	char *result;

	if (rand()%2){
		result = success;
	}else{
		result = failure;
	}

	generalControllerCreateTextWindow((generalController *)param1, result);

	return (int)(result != success);

}

int cmd_reload(void *param1, void *param2, char *success, char *failure){

	gdsl_list_remove(getPlayerBackpack(getGamePlayer((Game *)param1)),pointerCmp,param2);
	return plogic_reload((Game *)param1,(Object *)param2);

}

int cmd_potion(void *param1, void *param2, char *success, char *failure){

	Player *player = getGamePlayer((Game *)param1);
	Object *objeto = param2;
	if (player->life == MAX_LIFE){
		return ERR;
	}
	player->life += objeto->amount;
	if (player->life > MAX_LIFE){
		player->life = MAX_LIFE;	
	}

	gdsl_list_remove(getPlayerBackpack(player), pointerCmp, objeto);

	statusControllerDisplayMessage("Te has curado");
	return OK;
}



int cmd_default(void *param1){

	generalControllerCreateTextWindow((generalController *)param1, "Este comando no existe");

	return 0;
}

int cmd_help(void *param1, void *param2, char *success, char *failure){

	generalControllerCreateTextWindow((generalController *)param1, "Buttons:\na->shoot\nm->menu\nc->command window\nspace->carry object\nf->keep object\n¡Corre que te comen los Zombies!");

	return 0;
}


int cmd_MOAB(void *param1, void *param2, char *success, char *failure){
	gdsl_list_t enemies=getGameEnemies(((generalController *)param1)->game);
	int i;
	Enemie * enemie;
	if(!gdsl_list_get_size(enemies))
		generalControllerCreateTextWindow((generalController *)param1, failure);
	else{
		gdsl_list_flush(enemies);
		generalControllerCreateTextWindow((generalController *)param1, success);
	}
}
