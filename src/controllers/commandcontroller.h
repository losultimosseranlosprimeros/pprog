/* Se encarga de controlar el modalwindow correspondiente a la ventana
 * de comandos. */

#include "generalcontroller.h"
#include "../interface/commandwindow.h"
void commandControllerStartWithDefault(windowController *wc);

void commandControllerProcessChar(windowController *wc, int character);
/* Recibe un window controller (que es un commandcontroller) y interpreta el caracter pulsado
 * llamando a la función de generalController correspondiente. */

/* Si el caracter es distinto de ESC y de \n lo escribe,
	si es \n llama a crear una ventana de texto con el comando que haya.
	si es esc tiene que salir llamando a una función de superwindow. superwindowExitCurrentModal(superwindow *sw).

	(todo esto son llamadas a funciones de general controller con el general controller que hay en la estructura de windowcontroller)

*/
