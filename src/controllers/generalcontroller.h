/* Controla el proceso general de la aplicacion
 * Es el paso intermedio entre los distintos controladores
 * de interfaz y el mundo. */
#ifndef _generalcontrollerheader
#define _generalcontrollerheader

#include "../interface/modalwindow.h"
#include "../commandInterpreter/commandlist.h"
#include "../game/world.h"
#include "../game/game.h"
#include "commands.h"

typedef struct{
	void * sw;
	CmdList * commandList;
	World * world;
	Game * game;
}generalController;

#include "windowcontroller.h"

generalController * generalControllerStart();
 /* start 
  * Inicia el juego. Crea un superwindow y muestra en él
  * un mapWindow con su controlador correspondiente.
  */
 
bool generalControllerMainLoop(generalController *gc);

void generalControllerCreateDefaultMenu( generalController *gc );
/* Crea un menu general */
void generalControllerCreateObjectsMenu( generalController *gc );


void generalControllerCreateCommandWindow( generalController *gc );
/* crea una ventana de comandos */

void generalControllerCreateTextWindow(generalController *gc, char *text);
/* Crea una ventana de texto con el texto especificado */

void generalControllerCloseWindow(generalController *gc);

void freeGeneralController(generalController *gc);

#endif
